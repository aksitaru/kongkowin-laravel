<?php

namespace Database\Seeders;

use App\Models\User\UserStatus;
use Illuminate\Database\Seeder;

class UserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [ 'name' => 'Aktif' ],
            [ 'name' => 'Menunggu Verifikasi' ],
            [ 'name' => 'Banned' ],
            [ 'name' => 'Dihapus' ],
        ];

        foreach ($datas as $data) {
            UserStatus::create($data);
        }
    }
}
