<?php

namespace Database\Seeders;

use App\Models\User\UserProductRewardStatus;
use Illuminate\Database\Seeder;

class UserProductRewardStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [ 'name' => 'Menunggu Verifikasi' ],
            [ 'name' => 'Berhasil' ],
            [ 'name' => 'Batal' ],
        ];

        foreach ($datas as $data) {
            UserProductRewardStatus::create($data);
        }
    }
}
