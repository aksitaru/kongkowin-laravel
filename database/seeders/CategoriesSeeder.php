<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [ 'name' => 'Rumah Makan' ],
            [ 'name' => 'Kafe' ],
            [ 'name' => 'Kedai Kopi' ],
            [ 'name' => 'Kedai Teh' ],
            [ 'name' => 'Restoran' ],
            [ 'name' => 'Hidangan Laut' ],
        ];

        foreach ($datas as $data) {
            Category::create($data);
        }
    }
}
