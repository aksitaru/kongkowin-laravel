<?php

namespace Database\Seeders;

use App\Models\Admin\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'role_id' => 1,
                'username' => 'supardi',
                'email' => 'cafcforce0@gmail.com',
                'password' => Hash::make('pardi123'),
                'name' => 'Supardi'
            ],
            [
                'role_id' => 1,
                'username' => 'dimas',
                'email' => 'dimas@gmail.com',
                'password' => Hash::make('dimas123'),
                'name' => 'Supardi'
            ],
        ];

        foreach ($datas as $data) {
            Admin::create($data);
        }
    }
}
