<?php

namespace Database\Seeders;

use App\Models\Admin\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['name' => 'Super Admin'],
            ['name' => 'Admin'],
        ];

        foreach ($datas as $data) {
            Role::create($data);
        }
    }
}
