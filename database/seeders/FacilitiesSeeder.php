<?php

namespace Database\Seeders;

use App\Models\Facility;
use Illuminate\Database\Seeder;

class FacilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [ 'name' => 'Wifi' ],
            [ 'name' => 'Musik' ],
            [ 'name' => 'Musholla' ],
            [ 'name' => 'Toilet' ],
            [ 'name' => 'Parkir' ],
            [ 'name' => 'CCTV' ],
        ];

        foreach ($datas as $data) {
            Facility::create($data);
        }
    }
}
