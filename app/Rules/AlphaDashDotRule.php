<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlphaDashDotRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        return preg_match('/^[0-9A-Za-z.\-_]+$/u', $value) > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Format kolom :attribute hanya boleh diisi dengan huruf, angka, strip, underscores dan titik.';
    }
}
