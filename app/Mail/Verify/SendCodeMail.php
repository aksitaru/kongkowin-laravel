<?php

namespace App\Mail\Verify;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    // https://www.niagahoster.co.id/blog/cara-kirim-email-laravel/

    public $code;
    public $instagram = 'aksitaru.id';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@kongkow.aksitaru.org', 'Kongkow.in')->subject('Verifikasi Akun Kongkow (' . $this->code . ')')->view('email.verify.send_code');
    }
}
