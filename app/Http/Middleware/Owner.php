<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $placeDomain = $request->route()->parameters()['placeDomain'];

        $isAdmin = false;
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) $isAdmin = true;
        }

        if ($isAdmin) {
            return $next($request);
        } else {
            return redirect(route('user.myplace'));
        }
    }
}
