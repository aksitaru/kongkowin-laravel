<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseJsonHelpers;
use App\Models\Place\Place;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlaceController extends Controller
{
    public function show(Place $place) {
        return view('place.show', [
            'title' => $place->name,
            'place' => $place
        ]);
    }

    public function add_rate(Request $request, Place $place) {
        if (!auth()->check()) {
            $json = ResponseJsonHelpers::formError("Gagal Rate", "Belum Login")->addData('login', false);

            return response()->json($json->get(), 400);
        }

        try {
            DB::beginTransaction();
            $rates = $place->rates()->where('user_id', auth()->user()->id)->limit(1)->get();

            if ($rates->count() == 0) {
                // Create rate
                $rate = new Rate();
                $rate->user_id = auth()->user()->id;
                $rate->rating = $request->rating;
    
                $place->rates()->save($rate);
            } else {
                // Update rate
                $rate = $rates[0];
                $rate->rating = $request->rating;
                $rate->save();
            }
            DB::commit();

            $success = ResponseJsonHelpers::formSuccess("Sukses Rate", "Sukses Rate");
            return response()->json($success->get());
        } catch (\Throwable $th) {
            DB::rollback();
            //throw $th;
            $error = ResponseJsonHelpers::formError("Gagal Rate", $th->getMessage());
            return response()->json($error->get(), 400);
        }
    }
}
