<?php

namespace App\Http\Controllers\Owner;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    function getPlace($placeDomain) {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function index($placeDomain) {
        $place = $this->getPlace($placeDomain);

        return view('owner.profile.index', [
            'place' => $place,
            'title' => 'Profile ' . $place->name
        ]);
    }

    public function uploadLogo(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,png|max:2300',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 400);
        }

        try {
            DB::beginTransaction();

            $path = $request->file->store('image/place', 'public');

            $place->logo_path = $path;
            $place->save();

            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Ganti Logo Tempat Berhasil', '');
            $success->redirect(route('owner.product.profile', $placeDomain));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);

            $error = ResponseJsonHelpers::formError('Ganti Logo Tempat Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get(), 400);    
        }
    }

    public function upload(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,png|max:2300',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 400);
        }

        try {
            DB::beginTransaction();

            $path = $request->file->store('image/place', 'public');

            $uploadImage = new UploadImage([
                'file_path' => $path
            ]);

            $place->images()->save($uploadImage);

            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Ganti Logo Tempat Berhasil', '');
            $success->redirect(route('owner.product.profile', $placeDomain));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);

            $error = ResponseJsonHelpers::formError('Ganti Logo Tempat Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get(), 400);    
        }
    }
}
