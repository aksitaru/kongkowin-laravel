<?php

namespace App\Http\Controllers\Owner;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\ProductReward;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProductRewardController extends Controller
{
    function getPlace($placeDomain) {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function get($placeDomain) {
        $place = $this->getPlace($placeDomain);

        return view('owner.product.reward', [
            'place' => $place,
            'title' => 'Reward'
        ]);
    }

    public function show(Request $request, $placeDomain, ProductReward $product_reward) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        return response()->json($product_reward->with('product')->where('id', $product_reward->id)->first());
    }

    public function list(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        if ($request->ajax()) {
            $data = ProductReward::with('product')->whereRelation('product', 'place_id', '=', $place->id);
            
            return DataTables::of($data)
                ->editColumn('created_at', function ($row) {
                    return Carbon::parse($row->created_at)->format('Y-m-d H:i:s');
                })
                ->addIndexColumn()
                ->addColumn('thumbnail', function($row){
                    $image = asset('images/empty.jpg');
                    if ($row->product->images()->count() > 0) {
                        $image = "/storage/".$row->product->images()->select('file_path')->limit(1)->get()->pluck('file_path')->toArray()[0];
                    }
                    $imgThumbnail = '<img src="'. $image .'" class="img-thumbnail img-80" />';
                    return $imgThumbnail;
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button data-id='. $row->id .' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id='. $row->id .' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'thumbnail'])
                ->make(true);
        }
    }

    public function add(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'product_id' => 'numeric|exists:products,id|required|string',
            'points' => 'numeric|min:1'
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $product_id = $request->product_id;
            $points = $request->points;

            $product_rewards = $place->productRewards()->where('product_id', $product_id)->limit(1)->get();

            if ($product_rewards->count() == 0) {
                $product = $place->products()->find($product_id);
                $product_reward = new ProductReward;
                $product_reward->points = $points;
    
                $product->productReward()->save($product_reward);
            } else {
                $error = ResponseJsonHelpers::formError('Menambah Reward Gagal', 'Produk sudah dijadikan reward sebelumnya');

                return response()->json($error->get());    
            }

            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menambah Reward Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menambah Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }
    }

    
    public function update(Request $request, $placeDomain, ProductReward $product_reward)
    {
        $place = $this->getPlace($placeDomain);

        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'product_id' => 'numeric|exists:products,id|required|string',
            'points' => 'numeric|min:1'
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $product_id = $request->product_id;
            $points = $request->points;

            $product_rewards = $place->productRewards()->where('product_id', $product_id)->limit(1)->get();

            //cek jika edit point saja
            if ($product_rewards->count() > 0 && $product_reward->product_id == $product_id) {
                $product_reward->points = $points;
                $product_reward->save();
            } else {
                // cek jika dia ganti product dgn produk yg belum ada
                if ($product_rewards->count() == 0) {
                    $product_reward->product_id = $product_id;
                    $product_reward->points = $points;
                    $product_reward->save();
                } else {
                    $error = ResponseJsonHelpers::formError('Menambah Reward Gagal', 'Produk sudah dijadikan reward sebelumnya');

                    return response()->json($error->get());
                }
            }

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Mengedit Reward Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function delete(Request $request, $placeDomain, ProductReward $product_reward) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        try {
            DB::beginTransaction();

            $product_reward->delete();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menghapus Reward Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }
}
