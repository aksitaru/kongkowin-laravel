<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BarcodeController extends Controller
{
    // https://www.positronx.io/how-to-quickly-generate-barcode-in-laravel-application/

    function getPlace($placeDomain)
    {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function index($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        if ($place->code_visit == null) {
            $code = Str::upper(Str::random(6));

            return redirect(route('owner.barcode.next', $place->domain));
        } else {
            $code = $place->code_visit;
        }

        return view('owner.barcode', [
            'place' => $place,
            'title' => 'Barcode ' . $place->name,
            'code' => $code,
        ]);
    }

    public function next($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        $code = Str::upper(Str::random(6));

        try {
            DB::beginTransaction();
            $place->code_visit = $code;
            $place->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();

            return 'Error';
        }

        return redirect(route('owner.barcode', $placeDomain));
    }
}
