<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Models\Place\Place;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard($placeDomain)
    {
        $place = Place::where([
            'user_id' => auth()->user()->id,
            'domain' => $placeDomain
        ])->first();

        if ($place == null) {
            return redirect(route('user.myplace'));
        }

        return view('owner.dashboard', [
            'place' => $place,
            'title' => 'Dashboard'
        ]);
    }
}
