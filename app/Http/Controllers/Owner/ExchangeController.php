<?php

namespace App\Http\Controllers\Owner;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\User\UserProductReward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ExchangeController extends Controller
{
    function getPlace($placeDomain)
    {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function get($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        return view('owner.exchange.index', [
            'place' => $place,
            'title' => 'Exchange Point'
        ]);
    }

    public function show(Request $request, $placeDomain, UserProductReward $user_product_reward)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Reward Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        return response()->json($user_product_reward->with('product')->where('id', $user_product_reward->id)->first());
    }

    public function list(Request $request, $placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        if ($request->ajax()) {
            // $data = $place->userProductRewards()->with('product_reward');
            // $data = $place->userProductRewards()->with('product_reward', 'product_reward.product', 'user', 'user_product_reward_status');
            $data = UserProductReward::with('product_reward', 'product_reward.product', 'user', 'user_product_reward_status')->whereRelation('product_reward.product', 'place_id', '=', $place->id);

            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($row) {
                    return Carbon::parse($row->created_at)->format('Y-m-d H:i:s');
                })
                ->addColumn('product_html', function ($row) {
                    $image = asset('images/empty.jpg');
                    if ($row->product_reward->product->images()->count() > 0) {
                        $image = "/storage/" . $row->product_reward->product->images()->select('file_path')->limit(1)->get()->pluck('file_path')->toArray()[0];
                    }
                    $imgThumbnail = '<img src="' . $image . '" class="img-thumbnail img-80 mr-2" />' . $row->product_reward->product->name;
                    return $imgThumbnail;
                })
                ->addColumn('status_html', function ($row) {
                    $status_id = $row->user_product_reward_status_id;
                    $class_name = 'badge-danger';
                    switch ($status_id) {
                        case 1:
                            $class_name = 'badge-warning';
                            break;

                        case 2:
                            $class_name = 'badge-success';
                            break;

                        case 3:
                            $class_name = 'badge-danger';
                            break;

                        default:
                            # code...
                            break;
                    }
                    $statusHtml = '<h5><span class="badge ' . $class_name . '">' . $row->user_product_reward_status->name . '</span></h5>';
                    return $statusHtml;
                })
                ->addColumn('action', function ($row) {
                    if($row->user_product_reward_status_id == 1) {
                        $actionBtn = '<button data-id=' . $row->id . ' id="btn-cancel-row" class="delete btn btn-danger btn-sm mr-1">Batal</button><button data-id=' . $row->id . ' id="btn-accept-row" class="edit btn btn-success btn-sm">Konfirmasi</button>';
                    } else {
                        $actionBtn = '';
                    }
                    return $actionBtn;
                })
                ->rawColumns(['action', 'product_html', 'status_html'])
                ->make(true);
        }
    }

    public function accept(Request $request, $placeDomain, UserProductReward $user_product_reward)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Gagal Konfirmasi', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        try {
            DB::beginTransaction();

            $user_product_reward->user_product_reward_status_id = 2;
            $user_product_reward->save();

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Berhasil Konfirmasi', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Gagal Konfirmasi', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function cancel(Request $request, $placeDomain, UserProductReward $user_product_reward)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Gagal Membatalkan', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        try {
            DB::beginTransaction();

            $user_product_reward->user_product_reward_status_id = 3;
            $user_product_reward->save();

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Berhasil Membatalkan', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Gagal Membatalkan', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }
}
