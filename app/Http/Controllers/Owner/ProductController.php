<?php

namespace App\Http\Controllers\Owner;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\ProductTag;
use App\Models\Tag;
use App\Models\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    function getPlace($placeDomain)
    {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function select2($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        $query = request()->get('q');

        $data = $place->products()->select('id', 'name as text')->where('name', 'like', '%' . $query . '%')->get()->toArray();

        return response()->json($data);
    }

    public function get($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        return view('owner.product.product', [
            'place' => $place,
            'title' => 'Produk'
        ]);
    }

    public function viewCreate($placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        return view('owner.product.create', [
            'place' => $place,
            'title' => 'Tambah Produk'
        ]);
    }

    public function viewUpdate($placeDomain, Product $product)
    {
        $place = $this->getPlace($placeDomain);

        return view('owner.product.update', [
            'place' => $place,
            'product' => $product,
            'title' => 'Edit Produk ' . $product->name
        ]);
    }

    public function upload(Request $request, $placeDomain, Product $product)
    {
        $place = $this->getPlace($placeDomain);

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,png|max:2300',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 400);
        }

        try {
            DB::beginTransaction();

            $path = $request->file->store('image/product', 'public');

            $uploadImage = new UploadImage([
                'file_path' => $path
            ]);

            $product->images()->save($uploadImage);

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Upload Foto Produk Berhasil', '');
            $success->redirect(route('owner.product.update', [$placeDomain, $product]));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);

            $error = ResponseJsonHelpers::formError('Upload Foto Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get(), 400);
        }
    }

    public function show(Request $request, $placeDomain, Product $product)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        return response()->json($product);
    }

    public function list(Request $request, $placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        if ($request->ajax()) {
            // $data = Product::with('place');
            $data = $place->products();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('thumbnail', function ($row) {
                    $image = asset('images/empty.jpg');
                    if ($row->images()->count() > 0) {
                        $image = "/storage/" . $row->images()->select('file_path')->limit(1)->get()->pluck('file_path')->toArray()[0];
                    }
                    $imgThumbnail = '<img src="' . $image . '" class="img-thumbnail img-80" />';
                    return $imgThumbnail;
                })
                ->addColumn('tags', function ($row) {
                    $tagLabel = "";
                    foreach ($row->tags as $tag) {
                        $tagId = $tag->tag_id;

                        $product_tag = Tag::find($tagId);
                        $tagLabel .= '<span class="d-inine badge badge-primary">' . $product_tag->name . '</span>';
                    }
                    return $tagLabel;
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<button data-id=' . $row->id . ' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id=' . $row->id . ' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['thumbnail', 'tags', 'action'])
                ->make(true);
        }
    }

    public function create(Request $request, $placeDomain)
    {
        $place = $this->getPlace($placeDomain);

        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required|string|min:10',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $product = new Product([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'description' => $request->description,
                'price' => $request->price
            ]);

            $place->products()->save($product);

            if ($request->tags != null) {
                $tags = [];
                foreach ($request->tags as $tag) {
                    $tag = new ProductTag([
                        'tag_id' => $tag,
                    ]);
                    array_push($tags, $tag);
                }

                $product->tags()->saveMany($tags);
            }

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Menambah Produk Berhasil', '');
            $success->redirect(route('owner.product.update', [$placeDomain, $product]));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menambah Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function update(Request $request, $placeDomain, Product $product)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required|string|min:10',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->save();

            if ($request->tags != null) {
                $tags = [];
                foreach ($request->tags as $tag) {
                    $tag = new ProductTag([
                        'tag_id' => $tag,
                    ]);
                    array_push($tags, $tag);
                }
            }

            $product->tags()->delete();
            $product->tags()->saveMany($tags);

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Memperbarui Produk Berhasil', '');
            $success->redirect(route('owner.product.update', [$placeDomain, $product]));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function delete(Request $request, $placeDomain, Product $product)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        try {
            DB::beginTransaction();

            $product->delete();
            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Menghapus Produk Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Produk Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }
}
