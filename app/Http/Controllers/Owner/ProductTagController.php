<?php

namespace App\Http\Controllers\Owner;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProductTagController extends Controller
{    
    function getPlace($placeDomain) {
        foreach (auth()->user()->places as $key => $value) {
            if ($value->domain == $placeDomain) return $value;
        }

        return null;
    }

    public function select2($placeDomain) {
        $place = $this->getPlace($placeDomain);

        $query = request()->get('q');
        
        $data = $place->tags()->select('id', 'name as text')->where('name', 'like', '%'.$query.'%')->get()->toArray();

        return response()->json($data);
    }

    public function get($placeDomain) {
        $place = $this->getPlace($placeDomain);

        return view('owner.product.tag', [
            'place' => $place,
            'title' => 'Etalase'
        ]);
    }

    public function show(Request $request, $placeDomain, Tag $tag) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        return response()->json($tag);
    }

    public function list(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        if ($request->ajax()) {
            $data = $place->tags()->withCount('products');
            
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button data-id='. $row->id .' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id='. $row->id .' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function add(Request $request, $placeDomain) {
        $place = $this->getPlace($placeDomain);

        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $tag = new Tag([
                'name' => $request->name,
            ]);

            $place->tags()->save($tag);
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menambah Etalase Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menambah Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }
    }

    public function update(Request $request, $placeDomain, Tag $tag) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $tag->name = $request->name;
            $tag->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Memperbarui Etalase Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }

    public function delete(Request $request, $placeDomain, Tag $tag) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        try {
            DB::beginTransaction();

            $tag->delete();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menghapus Etalase Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Etalase Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }

}
