<?php

namespace App\Http\Controllers\Admin\Table;

use App\Models\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DataTables;
use App\Helpers\ResponseJsonHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function get() {
        return view('admin.table.user', [
            'title' => 'Tabel Pengguna'
        ]);
    }

    public function show(Request $request, User $user) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Pengguna Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        return response()->json($user);
    }

    public function list(Request $request) {
        if ($request->ajax()) {
            $data = user::withCount('places');

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button data-id='. $row->id .' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id='. $row->id .' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function update(Request $request, User $user) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Pengguna Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $user->name = $request->name;
            $user->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Memperbarui Pengguna Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Pengguna Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }

    public function delete(Request $request, User $user) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Pengguna Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        try {
            DB::beginTransaction();

            $user->places()->delete();
            $user->delete();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menghapus Pengguna Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Pengguna Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }
}
