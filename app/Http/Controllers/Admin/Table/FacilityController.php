<?php

namespace App\Http\Controllers\Admin\Table;

use App\Models\Facility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ResponseJsonHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class FacilityController extends Controller
{
    public function get() {
        return view('admin.table.facility', [
            'title' => 'Tabel Fasilitas'
        ]);
    }

    public function show(Request $request, Facility $facility) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        return response()->json($facility);
    }

    public function list(Request $request) {
        if ($request->ajax()) {
            $data = Facility::withCount('places');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button data-id='. $row->id .' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id='. $row->id .' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function add(Request $request) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $facility = new Facility;
            $facility->name = $request->name;
            $facility->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menambah Fasilitas Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menambah Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }
    }

    public function update(Request $request, Facility $facility) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $facility->name = $request->name;
            $facility->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Memperbarui Fasilitas Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }

    public function delete(Request $request, Facility $facility) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        try {
            DB::beginTransaction();

            $facility->places()->delete();
            $facility->delete();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menghapus Fasilitas Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Fasilitas Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }
}
