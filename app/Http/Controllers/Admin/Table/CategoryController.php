<?php

namespace App\Http\Controllers\Admin\Table;

use App\Models\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DataTables;
use App\Helpers\ResponseJsonHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function get() {
        return view('admin.table.category', [
            'title' => 'Tabel Kategori'
        ]);
    }

    public function show(Request $request, Category $category) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Mengakses Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        return response()->json($category);
    }

    public function list(Request $request) {
        if ($request->ajax()) {
            $data = Category::withCount('places');

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button data-id='. $row->id .' id="btn-edit-row" class="edit btn btn-success btn-sm">Edit</button> <button data-id='. $row->id .' id="btn-delete-row" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function add(Request $request) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $category = new Category;
            $category->name = $request->name;
            $category->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menambah Kategori Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menambah Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }
    }

    public function update(Request $request, Category $category) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menambah Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;
            
            collect($validator->errors())->each(function ($item, $key) use($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $category->name = $request->name;
            $category->save();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Memperbarui Kategori Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Memperbarui Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }

    public function delete(Request $request, Category $category) {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Menghapus Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

        try {
            DB::beginTransaction();

            $category->places()->delete();
            $category->delete();
            DB::commit();
            
            $success = ResponseJsonHelpers::formSuccess('Menghapus Kategori Berhasil', '');
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Menghapus Kategori Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());    
        }

    }
}
