<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    // https://www.kawankoding.id/membuat-fitur-multi-auth-di-laravel-7/
    use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    public function login()
    {
        if (auth()->guard('admin')->check()) {
            return redirect(route('admin.dashboard'));
        }

        return view('admin.auth.login', [
            'title' => 'Login'
        ]);
    }

    public function authLogin(Request $request)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Login Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);

            $success = ResponseJsonHelpers::formSuccess('Login Berhasil', '');
            $success->redirect(route('admin.dashboard'));
            return response()->json($success->get(), 200);
        } else {
            $this->incrementLoginAttempts($request);

            $error = ResponseJsonHelpers::formError('Login Gagal', 'Username atau Password salah');
            return response()->json($error->get(), 500);
        }
    }

    public function authLogout()
    {
        auth()->guard('admin')->logout();

        $success = ResponseJsonHelpers::formSuccess('Logout Berhasil', '');
        $success->redirect(route('admin.login'));
        return response()->json($success->get(), 200);
    }
}
