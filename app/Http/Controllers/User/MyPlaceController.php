<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\{Category, Facility};
use App\Models\Place\{Place, PlaceCategory, PlaceFacility};
use App\Rules\AlphaDashDotRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MyPlaceController extends Controller
{
    public function myplace()
    {
        $user = Auth::user();

        $places = $user->places;

        if (count($places) == 0) {
            return redirect('myplace/create');
        }
        
        return view('user.myplace.list', [
            'title' => 'Tempat Anda',
            'places' => $places,
        ]);
    }

    public function create()
    {
        $data = [
            'title' => 'Tambah Tempat',
            'p_cat' => Category::get(),
            'p_fac' => Facility::get()
        ];
        return view('user.myplace.create', $data);
    }

    public function postCreate(Request $request)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Membuat Tempat Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:60',
            'domain' => ['required', 'unique:places,domain', 'string', 'min:4', 'max:30', new AlphaDashDotRule],
            'categories' => 'required|array',
            'categories.*' => 'numeric|exists:categories,id',
            'address' => 'required|string|min:4|max:150',
            'description' => 'required|string|min:20',
            'facilities' => 'required|array',
            'facilities.*' => 'numeric|exists:facilities,id',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            // $categories = [];
            // foreach ($request->categories as $category) {
            //     array_push($categories, new PlaceCategory(['category_id' => $category]));
            // }
    
            // $facilities = [];
            // foreach ($request->facilities as $facility) {
            //     array_push($categories, new PlaceFacility(['facility_id' => $facility]));
            // }
    
            $user_id = Auth::user()->id;
            $place = Place::create([
                'user_id' => $user_id,
                'name' => $request->name,
                'domain' => $request->domain,
                'description' => $request->description,
                'address' => $request->address,
                'lat_lng_address' => json_encode([]),
            ]);
    
            $place->facilities()->attach($request->facilities);
            $place->categories()->attach($request->categories);

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Membuat Toko Berhasil', '');
            $success->redirect(route('owner.dashboard', $place->domain));
            return response()->json($success->get(), 200);
       } catch (\Exception $ex) {
            dd($ex);
            $error = ResponseJsonHelpers::formError('Membuat Toko Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function show(Place $place)
    {
        return view('myplace.show', [
            'title' => $place->name,
            'place' => $place
        ]);
    }

    public function edit(Place $place)
    {
        return view('myplace.edit', [
            'title' => 'Ubah ' . $place->name,
            'place' => $place,
            'p_cat' => Category::get(),
            'p_fac' => Facility::get()
        ]);
    }

    public function putEdit(Request $request, Place $place)
    {
        $request->validate([
            'name' => 'required|string|min:4|max:60',
            'domain' => 'required|string|alpha_dash|min:4|max:30|unique:places,domain,'.$place->id,
            'categories' => 'required|array',
            'categories.*' => 'numeric|exists:categories,id',
            'address' => 'required|string|min:4|max:60',
            'description' => 'required|string|min:20',
            'facilities' => 'required|array',
            'facilities.*' => 'numeric|exists:facilities,id',
        ]);

        // checking category id
        $categories = [];
        foreach ($request->categories as $category) {
            array_push($categories, new PlaceCategory(['category_id' => $category]));
        }

        $facilities = [];
        foreach ($request->facilities as $facility) {
            array_push($facilities, new PlaceFacility(['facility_id' => $facility]));
        }

        $place->name = $request->name;
        $place->domain = $request->domain;
        $place->address = $request->address;
        $place->description = $request->description;
        $place->save();

        $place->categories()->delete();
        $place->categories()->saveMany($categories);

        $place->facilities()->delete();
        $place->facilities()->saveMany($facilities);

        return redirect(route('myplace.edit', $place))->with('success', 'Berhasil Ubah Tempat');
    }

    public function photos(Place $place)
    {
        return view('myplace.photos', [
            'title' => 'Foto ' . $place->name,
            'place' => $place
        ]);
    }

    public function add_photo(Request $request, Place $place)
    {
        $request->validate([
            'name' => 'required|string|min:4|max:60',
            'file' => 'required|string',
        ]);

        
    }
}
