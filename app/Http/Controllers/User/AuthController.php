<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Mail\Verify\SendCodeMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Str;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    public function login()
    {
        if (auth()->check()) {
            return redirect(url()->previous());
        }

        return view('user.auth.login', [
            'title' => 'Masuk'
        ]);
    }

    public function authLogin(Request $request)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Login Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        $username = request()->input('username');
        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if (Auth::attempt([$field => $username, 'password' => $request->password])) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);

            $success = ResponseJsonHelpers::formSuccess('Login Berhasil', '');

            // akun perlu verifikasi
            $user = auth()->user();
            if ($user->user_status_id == 2) {
                // kode kadaluarsa -> resend_code
                if ($user->send_code_at == null || Carbon::parse($user->send_code_at)->addMinutes(10)->lessThan(now())) {
                    $success->redirect(route('user.auth.resend_code'));
                } else {
                    $success->redirect(route('user.auth.verify'));
                }
            } else {
                $success->redirect(url()->previous());
            }

            return response()->json($success->get(), 200);
        } else {
            $this->incrementLoginAttempts($request);

            $error = ResponseJsonHelpers::formError('Login Gagal', 'Username atau Password salah');
            return response()->json($error->get(), 500);
        }
    }

    public function register()
    {
        if (auth()->check()) {
            return redirect(url()->previous());
        }

        return view('user.auth.register', [
            'title' => 'Daftar'
        ]);
    }

    public function authRegister(Request $request)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Daftar Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required|string|alpha_dash|unique:users',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'birthday' => 'required|string',
            'address' => 'required|string|min:10',
            'phone' => 'required|numeric|digits_between:11,13',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        try {
            DB::beginTransaction();

            $data = $request->all();

            User::create([
                'username' => Str::lower($data['username']),
                'name' => $data['name'],
                'email' => Str::lower($data['email']),
                'password' => Hash::make($data['password']),
                'birthday' => $data['birthday'],
                'address' => $data['address'],
                'phone' => $data['phone'],
            ]);

            DB::commit();

            if (Auth::attempt(['username' => $request->username, 'password' => $request->password], true)) {
                $request->session()->regenerate();
                $this->clearLoginAttempts($request);

                $success = ResponseJsonHelpers::formSuccess('Daftar Berhasil', '');
                $success->redirect(route('user.auth.resend_code'));
                return response()->json($success->get(), 200);
            } else {
                $this->incrementLoginAttempts($request);

                $error = ResponseJsonHelpers::formError('Daftar Gagal', 'Username atau Password salah');
                return response()->json($error->get(), 500);
            }
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Daftar Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function verify()
    {
        $user = auth()->user();
        $user_status_id = $user->user_status_id;

        $id_status_user_verified = 1;
        $id_status_pending_for_verification = 2;
        if ($user_status_id == $id_status_user_verified) {
            return redirect(url()->previous());
        }

        return view('user.auth.verify', [
            'title' => 'Verifikasi Email'
        ]);
    }

    public function authVerify(Request $request)
    {
        if (!$request->expectsJson()) {
            $error = ResponseJsonHelpers::formError('Verifikasi Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }

        $validator = Validator::make($request->all(), [
            'code' => 'required|string|size:6',
        ]);

        if ($validator->fails()) {
            $error = new ResponseJsonHelpers;

            collect($validator->errors())->each(function ($item, $key) use ($error) {
                $error->invalidInput($key, implode(',', $item));
            });

            return response()->json($error->get(), 500);
        }

        $user = auth()->user();

        // Akun telah verifikasi
        if ($user->user_status_id != 2) {
            $error = ResponseJsonHelpers::formError('Verifikasi Gagal', 'Akun anda tidak memerlukan verifikasi!');

            return response()->json($error->get());
        }

        // gagal kode salah
        if ($user->verification_code != $request->code) {
            $error = ResponseJsonHelpers::formError('Kode salah', 'Kode yang anda masukkan salah!');

            return response()->json($error->get());
        }

        // gagal kode kadaluarsa
        if (Carbon::parse($user->send_code_at)->addMinutes(10)->lessThan(now())) {
            $error = ResponseJsonHelpers::formError('Kode kadaluarsa', 'Silahkan tekan tombol <strong>Resend Code</strong> untuk mendapatkan kode baru!');

            return response()->json($error->get());
        }

        // jika sukses
        try {

            DB::beginTransaction();

            $user->email_verified_at = now();
            $user->user_status_id = 1;
            $user->save();

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Verifikasi Berhasil', '');
            $success->redirect(route('user.dashboard'));
            return response()->json($success->get(), 200);
        } catch (\Exception $ex) {
            DB::rollBack();

            dd($ex);
            $error = ResponseJsonHelpers::formError('Verifikasi Gagal', 'Terjadi Kesalahan');

            return response()->json($error->get());
        }
    }

    public function resendCode()
    {
        $user = auth()->user();
        $is_code_expired = $user->user_status_id == 2 && ($user->send_code_at == null || Carbon::parse($user->send_code_at)->addMinutes(2)->lessThan(now()));

        if ($is_code_expired) {
            try {
                DB::beginTransaction();

                $code = Str::upper(Str::random(6));

                $user->verification_code = $code;
                $user->send_code_at = now();

                $user->save();

                Mail::to($user->email)->send(new SendCodeMail($code));

                DB::commit();
            } catch (\Throwable $th) {
                dd($th);
            }
        }

        return redirect(route('user.auth.verify'));
    }

    public function authLogout()
    {
        auth()->logout();

        $success = ResponseJsonHelpers::formSuccess('Logout Berhasil', '');
        $success->redirect(route('user.dashboard'));
        return response()->json($success->get(), 200);
    }
}
