<?php

namespace App\Http\Controllers;

use App\Mail\MyTestMail;
use App\Mail\Verify\SendCodeMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class MailController extends Controller
{
    public function index()
    {
        try {
            $details = [
                'title' => 'Mail from websitepercobaan.com',
                'body' => 'This is for testing email using smtp'
            ];

            Mail::to('supardi.dev@gmail.com')->send(new MyTestMail($details));

            dd("Email sudah terkirim.");
        } catch (\Throwable $th) {
            dd($th);
        }
    }

    public function sendCode()
    {
        $code = Str::random(6);

        Mail::to('supardi.dev@gmail.com')->send(new SendCodeMail($code));

        dd("Email sudah terkirim.");
    }
}
