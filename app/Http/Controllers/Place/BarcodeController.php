<?php

namespace App\Http\Controllers\Place;

use App\Helpers\ResponseJsonHelpers;
use App\Http\Controllers\Controller;
use App\Models\Place\Place;
use App\Models\Product\ProductReward;
use App\Models\User\UserPlaceReward;
use App\Models\User\UserProductReward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class BarcodeController extends Controller
{
    public function claim(Place $place, $code_visit)
    {
        return view('place.barcode', [
            'title' => 'Claim Point',
            'place' => $place,
            'code_visit' => $code_visit
        ]);
    }

    public function postClaim(Request $request, Place $place, $code_visit)
    {
        if (!auth()->check()) {
            $json = ResponseJsonHelpers::error("Gagal Claim", "Belum Login")->addData('login', false);

            return response()->json($json->get(), 400);
        }

        if ($code_visit != $place->code_visit) {
            $json = ResponseJsonHelpers::formError("Gagal Claim", "QR Code Salah");

            return response()->json($json->get(), 400);
        }

        $has_today_claim = $place->userPlaceRewards()->where('user_id', auth()->user()->id)->whereDate('created_at', Carbon::today())->count() > 0;
        if ($has_today_claim) {
            $json = ResponseJsonHelpers::formError("Gagal Claim", "Anda sudah mendapatkan point untuk hari ini, anda bisa kembali lagi besok dan jangan scan barcode kembali. <span class=\"far fa-smile\"></span>");

            return response()->json($json->get(), 400);
        }

        try {
            DB::beginTransaction();

            // add point user
            $user_place_reward = new UserPlaceReward;
            $user_place_reward->user_id = auth()->user()->id;
            $user_place_reward->points = $place->point_visit;
            $place->userPlaceRewards()->save($user_place_reward);

            // change code visit
            $code = Str::upper(Str::random(6));

            $place->code_visit = $code;
            $place->save();

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Sukses Claim', 'Anda pendapatkan ' . $place->point_visit . ' point');
            $success->addData('claim_body', '<div class="alert alert-success" role="alert">Berhasil mendapatkan ' . $place->point_visit . ' point</div>');

            $sum_point_reward = $place->userPlaceRewards()->where('user_id', auth()->user()->id)->sum('points');
            $sum_point_minus_exchange = auth()->user()->userProductRewards()->where('user_id', auth()->user()->id)->where('user_product_reward_status_id', '!=', 3)->sum('points');
    
            $user_points = $sum_point_reward - $sum_point_minus_exchange;
    
            $success->addData('user_point', $user_points);
            
            return response()->json($success->get());
        } catch (\Throwable $th) {
            DB::rollback();
            //throw $th;
            $error = ResponseJsonHelpers::formError("Gagal Claim", $th->getMessage());
            return response()->json($error->get(), 400);
        }
    }

    public function postExchange(Request $request, Place $place)
    {
        if (!auth()->check()) {
            $json = ResponseJsonHelpers::formError("Gagal Tukar Point", "Belum Login")->addData('login', false);

            return response()->json($json->get(), 400);
        }

        $product_reward_id = $request->product_reward_id;

        $product_reward = $place->productRewards()->find($product_reward_id);
        if (!$product_reward) {
            $json = ResponseJsonHelpers::formError("Product Reward tidak ada", "");

            return response()->json($json->get(), 400);
        }

        $sum_point_reward = $place->userPlaceRewards()->where('user_id', auth()->user()->id)->sum('points');
        $sum_point_minus_exchange = auth()->user()->userProductRewards()->where('user_id', auth()->user()->id)->where('user_product_reward_status_id', '!=', 3)->sum('points');

        $user_points = $sum_point_reward - $sum_point_minus_exchange;

        if ($user_points < $product_reward->points) {
            $json = ResponseJsonHelpers::formError("Point anda tidak mencukupi!", "");

            return response()->json($json->get(), 400);
        }

        try {
            DB::beginTransaction();

            // add point user
            $user_product_reward = new UserProductReward;
            $user_product_reward->product_reward_id = $product_reward->id;
            $user_product_reward->points = $product_reward->points;
            auth()->user()->userProductRewards()->save($user_product_reward);

            DB::commit();

            $success = ResponseJsonHelpers::formSuccess('Sukses Tukar Point', 'Silahkan konfirmasi ke kasir '. $place->name . ' untuk mendapatkan produk rewardnya.');

            $sum_point_reward = $place->userPlaceRewards()->where('user_id', auth()->user()->id)->sum('points');
            $sum_point_minus_exchange = auth()->user()->userProductRewards()->where('user_id', auth()->user()->id)->where('user_product_reward_status_id', '!=', 3)->sum('points');
    
            $user_points = $sum_point_reward - $sum_point_minus_exchange;
            $success->addData('user_point', $user_points);
            
            return response()->json($success->get());
        } catch (\Throwable $th) {
            DB::rollback();
            //throw $th;
            $error = ResponseJsonHelpers::formError("Gagal Tukar Point", $th->getMessage());
            return response()->json($error->get(), 400);
        }
    }
}
