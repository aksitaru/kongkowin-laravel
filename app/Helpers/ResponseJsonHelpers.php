<?php

namespace App\Helpers;

class ResponseJsonHelpers {
    public static $__response = [];

    public static function error($message) {
        self::$__response['result'] = false;
        self::$__response['error']['message'] = $message;

        return new static;
    }

    public static function formError($head, $body) {
        self::$__response['result'] = false;
        self::$__response['form']['alert']['icon'] = 'error';
        self::$__response['form']['alert']['head'] = $head;
        self::$__response['form']['alert']['body'] = $body;

        return new static;
    }

    public static function formSuccess($head, $body) {
        self::$__response['result'] = true;
        self::$__response['form']['alert']['icon'] = 'success';
        self::$__response['form']['alert']['head'] = $head;
        self::$__response['form']['alert']['body'] = $body;

        return new static;
    }

    public static function validInput($name, $message) {
        self::$__response['form']['input'][$name]['type'] = 'valid';
        self::$__response['form']['input'][$name]['message'] = $message;

        return new static;
    }

    public static function invalidInput($name, $message) {
        self::$__response['result'] = false;
        self::$__response['form']['input'][$name]['type'] = 'invalid';
        self::$__response['form']['input'][$name]['message'] = $message;

        return new static;
    }

    public static function addData($name, $value) {
        self::$__response['data'][$name] = $value;

        return new static;
    }

    public static function redirect($url) {
        self::$__response['redirect'] = $url;

        return new static;
    }

    public static function get() {
        return self::$__response;
    }

}