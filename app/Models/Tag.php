<?php

namespace App\Models;

use App\Models\Product\ProductTag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(ProductTag::class);
    }

    public function tagable()
    {
        return $this->morphTo();
    }
}
