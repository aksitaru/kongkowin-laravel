<?php

namespace App\Models\Place;

use App\Models\Facility;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaceFacility extends Model
{
    use HasFactory;

    public $incrementing = false;
    public $timestamps = false;

    protected $guarded = [];

    public function place()
    {
        return $this->hasOne(Place::class);
    }

    public function facility()
    {
        return $this->hasOne(Facility::class);
    }
}
