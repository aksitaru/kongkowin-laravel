<?php

namespace App\Models\Place;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaceProduct extends Model
{
    use HasFactory;

    protected $guarded = [];
}
