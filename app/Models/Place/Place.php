<?php

namespace App\Models\Place;

use App\Models\Category;
use App\Models\Facility;
use App\Models\Product\Product;
use App\Models\Product\ProductReward;
use App\Models\Rate;
use App\Models\Tag;
use App\Models\UploadImage;
use App\Models\User\UserPlaceReward;
use App\Models\User\UserProductReward;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    // Owner

    public function facilities() {
       return $this->belongsToMany(Facility::class);
    }

    public function categories() {
        return $this->belongsToMany(Category::class);
    }

    public function photos() {
        return $this->hasMany(PlacePhoto::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

    public function productRewards() {
        return $this->hasManyThrough(ProductReward::class, Product::class);
    }

    public function userPlaceRewards() {
        return $this->hasMany(UserPlaceReward::class);
    }

    // public function userProductRewards() {
    //     return $this->hasManyThrough(UserProductReward::class, ProductReward::class);
    // }

    public function tags() {
        return $this->morphMany(Tag::class, 'tagable');
    }

    public function images() {
        return $this->morphMany(UploadImage::class, 'imageable');
    }


    // User
    public function rates() {
        return $this->hasMany(Rate::class);
    }
}
