<?php

namespace App\Models;

use App\Models\Place\Place;
use App\Models\User\UserPlaceReward;
use App\Models\User\UserProductReward;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'birthday',
        'address',
        'phone',
        'balance',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function rates() {
        return $this->hasMany(Rate::class);
    }

    public function userPlaceRewards()
    {
        return $this->hasMany(UserPlaceReward::class);
    }

    public function userProductRewards()
    {
        return $this->hasMany(UserProductReward::class);
    }

}
