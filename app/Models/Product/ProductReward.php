<?php

namespace App\Models\Product;

use App\Models\Place\Place;
use App\Models\User\UserPlaceReward;
use App\Models\User\UserProductReward;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReward extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function userPlaceReward()
    {
        return $this->hasMany(UserPlaceReward::class);
    }

    public function userProductReward()
    {
        return $this->hasMany(UserProductReward::class);
    }
}
