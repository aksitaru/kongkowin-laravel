<?php

namespace App\Models\Product;

use App\Models\Place\Place;
use App\Models\UploadImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function place() {
        return $this->hasOne(Place::class);
    }

    public function tags() {
        return $this->hasMany(ProductTag::class);
    }

    public function images() {
        return $this->morphMany(UploadImage::class, 'imageable');
    }

    public function productReward() {
        return $this->hasOne(ProductReward::class);
    }
}
