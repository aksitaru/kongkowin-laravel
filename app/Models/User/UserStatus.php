<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = [];

    public function users()
    {
        return $this->hasMany(User::class);
    }

}
