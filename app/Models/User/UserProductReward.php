<?php

namespace App\Models\User;

use App\Models\Product\ProductReward;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProductReward extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function product_reward() {
        return $this->belongsTo(ProductReward::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function user_product_reward_status()
    {
        return $this->belongsTo(UserProductRewardStatus::class);
    }

}
