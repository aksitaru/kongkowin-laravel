<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProductRewardStatus extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = [];

}
