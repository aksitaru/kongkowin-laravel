<?php

namespace App\View\Components\SideNav;

use Illuminate\View\Component;

class MyPlaceShow extends Component
{
    public $placeDomain;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($placeDomain)
    {
        $this->placeDomain = $placeDomain;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.side-nav.my-place-show');
    }
}
