<?php

namespace App\View\Components\Owner;

use Illuminate\View\Component;

class Sidebar extends Component
{
    public $placeDomain;
    public $places_deactive;
    public $place_active;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($placeDomain)
    {
        $places = auth()->user()->places;

        $place_active = null;
        $places_deactive = [];
        foreach ($places as $place) {
            if ($place->domain == $placeDomain) {
                $place_active = $place;
            } else {
                array_push($places_deactive, $place);
            }
        }

        $this->placeDomain = $placeDomain;
        $this->places_deactive = $places_deactive;

        $this->place_active = $place_active;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.owner.sidebar');
    }
}
