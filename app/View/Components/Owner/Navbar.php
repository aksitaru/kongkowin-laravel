<?php

namespace App\View\Components\Owner;

use App\Models\Place\Place;
use Illuminate\View\Component;

class Navbar extends Component
{
    public $placeDomain;
    public $places;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($placeDomain)
    {
        $this->placeDomain = $placeDomain;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.owner.navbar');
    }
}
