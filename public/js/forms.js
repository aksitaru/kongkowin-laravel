Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
});

$('form[method="post"]').submit(function (event) {
    // $.ajaxSetup({
    //     headers: {
    //         "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr("content"),
    //     },
    // });

    var url = this.action;
    var form = $(this);

    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(this),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function (data) {
            var inputs = $(":input", form);

            inputs.removeClass("is-valid");
            inputs.removeClass("is-invalid");

            var invalid = $("span.invalid-feedback", form);
            invalid.remove();

            var valid = $("span.valid-feedback", form);
            valid.remove();

            Swal.fire("Loading", "Mohon Tunggu", "");
            Swal.showLoading();
        },
        success: function (data) {
            Swal.close();

            responseHandler(form, data);
        },
        error: function (request) {
            data = request.responseJSON;

            responseHandler(form, data);
        },
    });
    event.preventDefault();
});

function responseHandler(form, data) {
    Swal.close();

    if (data.result == undefined) {
        Swal.fire("Error Request", "Coba beberapa saat lagi.", "error");
    } else {
        if (data.form.alert) {
            if (data.result) {
                form.trigger("reset");

                var modal = form.data("modal");
                if (modal != undefined) {
                    $(modal).modal("hide");
                }

                var table = form.data("table");
                if (table != undefined) {
                    $(table).DataTable().ajax.reload(null, false);
                }

                Toast.fire(
                    data.form.alert.head,
                    data.form.alert.body,
                    data.form.alert.icon
                );
            } else {
                Swal.fire(
                    data.form.alert.head,
                    data.form.alert.body,
                    data.form.alert.icon
                );
            }
        }

        if (data.form.input) {
            Object.keys(data.form.input).forEach((element) => {
                var input = $(":input[name='" + element + "']", form);
                var input_response = data.form.input[element];

                if (input_response.type == "invalid") {
                    input.addClass("is-invalid");
                    input
                        .parent()
                        .append(
                            '<span class="invalid-feedback" role="alert">' +
                                input_response.message +
                                "</span>"
                        );
                }
            });
        }

        if (data.redirect) {
            location.href = data.redirect;
        }
    }
}
