<?php

use App\Http\Controllers\{MailController, PlaceController};
use App\Http\Controllers\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\Table\{CategoryController, FacilityController, UserController};
use App\Http\Controllers\Owner\BarcodeController as OwnerBarcodeController;
use App\Http\Controllers\Owner\DashboardController as OwnerDashboardController;
use App\Http\Controllers\Owner\ExchangeController;
use App\Http\Controllers\Owner\OwnerController;
use App\Http\Controllers\Owner\ProductController;
use App\Http\Controllers\Owner\ProductRewardController;
use App\Http\Controllers\Owner\ProductTagController;
use App\Http\Controllers\Owner\ProfileController;
use App\Http\Controllers\User\AuthController as UserAuthController;
use App\Http\Controllers\User\DashboardController as UserDashboardController;
use App\Http\Controllers\User\MyPlaceController;
use App\Http\Controllers\Place\BarcodeController as PlaceBarcodeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| User here
|
*/

Route::get('mail/test', [MailController::class, 'index'])->name('mail.test');
Route::get('mail/verify/send_code', [MailController::class, 'sendCode'])->name('mail.send_code');
Route::get('mail/view', function() {
    return view('email.verify.send_code', [
        'instagram' => 'aksitaru.id',
        'code' => 'XX789A'
    ]);
})->name('mail.test');

Route::get('login', [UserAuthController::class, 'login'])->name('user.login');
Route::post('auth/login', [UserAuthController::class, 'authLogin'])->name('user.auth.login');

Route::get('register', [UserAuthController::class, 'register'])->name('user.register');
Route::post('auth/register', [UserAuthController::class, 'authRegister'])->name('user.auth.register');

Route::get('/', [UserDashboardController::class, 'dashboard'])->name('user.dashboard');
Route::middleware(['auth'])->get('myplace', [MyPlaceController::class, 'myplace'])->name('user.myplace');
Route::middleware(['auth:admin'])->get('admin', [AdminDashboardController::class, 'dashboard'])->name('admin.dashboard');
Route::get('{place:domain}', [PlaceController::class, 'show'])->name('place.show');
Route::post('{place:domain}/add_rate', [PlaceController::class, 'add_rate'])->name('place.add_rate');

Route::post('{place:domain}/barcode/exchange', [PlaceBarcodeController::class, 'postExchange'])->name('place.barcode.exchange');

Route::get('{place:domain}/barcode/{code_visit}', [PlaceBarcodeController::class, 'claim'])->name('place.barcode');
Route::post('{place:domain}/barcode/{code_visit}', [PlaceBarcodeController::class, 'postClaim']);

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::prefix('place')->group(function () {
//     Route::get('{place:domain}', [PlaceController::class, 'show'])->name('place.show');
// });

Route::middleware(['auth'])->group(function () {
    Route::get('user/verify', [UserAuthController::class, 'verify'])->name('user.auth.verify');
    Route::post('user/verify', [UserAuthController::class, 'authVerify']);
    Route::get('user/resend_code', [UserAuthController::class, 'resendCode'])->name('user.auth.resend_code');

    Route::post('auth/logout', [UserAuthController::class, 'authLogout'])->name('user.auth.logout');

    Route::get('myplace/create', [MyPlaceController::class, 'create'])->name('user.myplace.create');

    Route::post('myplace/create', [MyPlaceController::class, 'postCreate']);

    Route::prefix('owner/{placeDomain}')->middleware('owner')->group(function ($placeDomain) {
            Route::get('/', [OwnerDashboardController::class, 'dashboard'])->name('owner.dashboard');
            Route::get('/', [OwnerDashboardController::class, 'dashboard'])->name('owner.dashboard');

            Route::get('/profile', [ProfileController::class, 'index'])->name('owner.profile');
            Route::post('/profile/upload_logo', [ProfileController::class, 'uploadLogo'])->name('owner.profile.upload_logo');
            Route::post('/profile/upload', [ProfileController::class, 'upload'])->name('owner.profile.upload');

            Route::get('/barcode', [OwnerBarcodeController::class, 'index'])->name('owner.barcode');
            Route::get('/barcode/next', [OwnerBarcodeController::class, 'next'])->name('owner.barcode.next');

            Route::get('/exchange/', [ExchangeController::class, 'get'])->name('owner.exchange');
            Route::get('exchange/select2', [ExchangeController::class, 'select2'])->name('owner.exchange.select2');
            Route::post('exchange/list', [ExchangeController::class, 'list'])->name('owner.exchange.list');
            Route::post('exchange/add', [ExchangeController::class, 'add'])->name('owner.exchange.add');
            Route::post('exchange/show/{user_product_reward:id}', [ExchangeController::class, 'show'])->name('owner.exchange.show');
            Route::post('exchange/accept/{user_product_reward:id}', [ExchangeController::class, 'accept'])->name('owner.exchange.accept');
            Route::post('exchange/cancel/{user_product_reward:id}', [ExchangeController::class, 'cancel'])->name('owner.exchange.cancel');

            Route::get('/product', [ProductController::class, 'get'])->name('owner.product');
            Route::get('product/select2', [ProductController::class, 'select2'])->name('owner.product.select2');
            Route::post('product/list', [ProductController::class, 'list'])->name('owner.product.list');
            Route::get('product/create', [ProductController::class, 'viewCreate'])->name('owner.product.create');
            Route::post('product/create', [ProductController::class, 'create']);
            Route::post('product/show/{product:id}', [ProductController::class, 'show'])->name('owner.product.show');
            Route::get('product/update/{product:id}', [ProductController::class, 'viewUpdate'])->name('owner.product.update');
            Route::post('product/update/{product:id}', [ProductController::class, 'update']);
            Route::post('product/delete/{product:id}', [ProductController::class, 'delete'])->name('owner.product.delete');
            Route::post('product/upload/{product:id}', [ProductController::class, 'upload'])->name('owner.product.upload');

            Route::get('/product/tag/', [ProductTagController::class, 'get'])->name('owner.product.tag');
            Route::get('product/tag/select2', [ProductTagController::class, 'select2'])->name('owner.product.tag.select2');
            Route::post('product/tag/list', [ProductTagController::class, 'list'])->name('owner.product.tag.list');
            Route::post('product/tag/add', [ProductTagController::class, 'add'])->name('owner.product.tag.add');
            Route::post('product/tag/show/{tag:id}', [ProductTagController::class, 'show'])->name('owner.product.tag.show');
            Route::post('product/tag/update/{tag:id}', [ProductTagController::class, 'update'])->name('owner.product.tag.update');
            Route::post('product/tag/delete/{tag:id}', [ProductTagController::class, 'delete'])->name('owner.product.tag.delete');

            Route::get('/product/reward/', [ProductRewardController::class, 'get'])->name('owner.product.reward');
            Route::get('product/reward/select2', [ProductRewardController::class, 'select2'])->name('owner.product.reward.select2');
            Route::post('product/reward/list', [ProductRewardController::class, 'list'])->name('owner.product.reward.list');
            Route::post('product/reward/add', [ProductRewardController::class, 'add'])->name('owner.product.reward.add');
            Route::post('product/reward/show/{product_reward:id}', [ProductRewardController::class, 'show'])->name('owner.product.reward.show');
            Route::post('product/reward/update/{product_reward:id}', [ProductRewardController::class, 'update'])->name('owner.product.reward.update');
            Route::post('product/reward/delete/{product_reward:id}', [ProductRewardController::class, 'delete'])->name('owner.product.reward.delete');

    });

    // Route::prefix('myplace')->group(function () {
    //     Route::get('create', [MyPlaceController::class, 'create'])->name('myplace.create');
    //     Route::post('create', [MyPlaceController::class, 'store']);

    //     Route::get('/', [MyPlaceController::class, 'myplace'])->name('myplace');
    //     Route::prefix('{place:domain}')->group(function () {
    //         Route::get('/', [MyPlaceController::class, 'show'])->name('myplace.show');            

    //         Route::get('edit', [MyPlaceController::class, 'edit'])->name('myplace.edit');
    //         Route::put('edit', [MyPlaceController::class, 'putEdit']);

    //         Route::get('photos', [MyPlaceController::class, 'photos'])->name('myplace.photos');
    //         Route::post('add_photo', [MyPlaceController::class, 'add_photos'])->name('myplace.add_photo');
    //     });
    // });
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| admin here
|
*/

Route::prefix('admin')->group(function () {
    Route::get('login', [AdminAuthController::class, 'login'])->name('admin.login');
    Route::post('auth/login', [AdminAuthController::class, 'authLogin'])->name('admin.auth.login');

    Route::middleware(['auth:admin'])->group(function () {
        Route::post('auth/logout', [AdminAuthController::class, 'authLogout'])->name('admin.auth.logout');


        // https://www.positronx.io/laravel-datatables-example/
        Route::prefix('table')->group(function () {
            Route::get('category', [CategoryController::class, 'get'])->name('admin.table.category');
            Route::post('category/list', [CategoryController::class, 'list'])->name('admin.table.category.list');
            Route::post('category/add', [CategoryController::class, 'add'])->name('admin.table.category.add');
            Route::post('category/show/{category:id}', [CategoryController::class, 'show'])->name('admin.table.category.show');
            Route::post('category/update/{category:id}', [CategoryController::class, 'update'])->name('admin.table.category.update');
            Route::post('category/delete/{category:id}', [CategoryController::class, 'delete'])->name('admin.table.category.delete');

            Route::get('facility', [FacilityController::class, 'get'])->name('admin.table.facility');
            Route::post('facility/list', [FacilityController::class, 'list'])->name('admin.table.facility.list');
            Route::post('facility/add', [FacilityController::class, 'add'])->name('admin.table.facility.add');
            Route::post('facility/show/{facility:id}', [FacilityController::class, 'show'])->name('admin.table.facility.show');
            Route::post('facility/update/{facility:id}', [FacilityController::class, 'update'])->name('admin.table.facility.update');
            Route::post('facility/delete/{facility:id}', [FacilityController::class, 'delete'])->name('admin.table.facility.delete');

            Route::get('user', [UserController::class, 'get'])->name('admin.table.user');
            Route::post('user/list', [UserController::class, 'list'])->name('admin.table.user.list');
            Route::post('user/add', [UserController::class, 'add'])->name('admin.table.user.add');
            Route::post('user/show/{user:id}', [UserController::class, 'show'])->name('admin.table.user.show');
            Route::post('user/update/{user:id}', [UserController::class, 'update'])->name('admin.table.user.update');
            Route::post('user/delete/{user:id}', [UserController::class, 'delete'])->name('admin.table.user.delete');
        });
    });
});
