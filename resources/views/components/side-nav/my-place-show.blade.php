<div class="list-group">
    <a href="/myplace/{{ $placeDomain }}" class="list-group-item list-group-item-action">Dashboard</a>
    <a href="/myplace/{{ $placeDomain }}/edit" class="list-group-item list-group-item-action">Ubah Tempat</a>
    <a href="/myplace/{{ $placeDomain }}/photos" class="list-group-item list-group-item-action">Foto Tempat</a>
    <a href="/myplace/{{ $placeDomain }}/products" class="list-group-item list-group-item-action">Produk Tempat</a>
    <a href="/myplace/{{ $placeDomain }}/rates" class="list-group-item list-group-item-action">Ulasan</a>
    <a href="/myplace/{{ $placeDomain }}/visitors" class="list-group-item list-group-item-action">Pengunjung</a>
</div>