<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('owner.dashboard', $placeDomain) }}" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Profile Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user"></i>
                <p class="d-none d-md-inline">{{ Auth::user()->name }}</p>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-header">
                    <h6 class="font-weight-bold">{{ Auth::user()->name }}</h6>
                    <p>User</p>
                </div>
                <div class="dropdown-divider"></div>
                <a onclick="event.preventDefault();
                            $('#logout-form').submit();" class="dropdown-item dropdown-footer">
                    <i class="fas fa-sign-out"></i>
                    Logout
                </a>
                <form id="logout-form" action="{{ route('user.auth.logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
