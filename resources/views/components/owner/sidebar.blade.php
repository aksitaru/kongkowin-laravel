  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{ route('owner.dashboard', $placeDomain) }}" class="brand-link">
          <span class="brand-text font-weight-bold">{{ config('app.name', 'Kongkow') }} <small>Owner</small> </span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex align-items-center">
              <a href="{{ route('owner.profile', $place_active->domain) }}" class="d-block">
                  <div class="image">
                      @if ($place_active->logo_path != null)
                          <img src="/storage/{{ $place_active->logo_path }}" class="img-circle elevation-2">
                      @else
                          <img src="{{ asset('images/empty.jpg') }}" class="img-circle elevation-2">
                      @endif
                  </div>
                  <div class="info">
                      <a href="{{ route('owner.profile', $place_active->domain) }}"
                          class="d-block">{{ $place_active->name }}
                          <div class="star a">
                              @php
                                  $rates = $place_active->rates;
                                  $rating_avg = number_format($rates->avg('rating'), 1);
                                  $rating_count = $rates->count();
                                  
                                  if ($rating_avg < 1.5) {
                                      $btnClass = 'btn-danger';
                                  } elseif ($rating_avg < 3.5) {
                                      $btnClass = 'btn-warning';
                                  } else {
                                      $btnClass = 'btn-success';
                                  }
                                  
                              @endphp
                              <button class="btn btn-xs {{ $btnClass }}">
                                  {{ $rating_avg }}
                                  <span class="fas fa-star fa-xs"></span>
                              </button>
                              <span class="ml-2">{{ $rating_count }} Ulasan</span>
                          </div>
                      </a>
                  </div>
              </a>
          </div>

          <!-- SidebarSearch Form -->
          <div class="form-inline">
              <div class="input-group" data-widget="sidebar-search">
                  <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                      aria-label="Search">
                  <div class="input-group-append">
                      <button class="btn btn-sidebar">
                          <i class="fas fa-search fa-fw"></i>
                      </button>
                  </div>
              </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                  {{-- <li class="nav-item">
                      <a href="{{ route('owner.dashboard', $place_active->domain) }}" class="nav-link">
                          <i class="nav-icon fas fa-home"></i>
                          <p>
                              {{ $place_active->name }}
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          @foreach ($places_deactive as $place)
                              <li class="nav-item">
                                  <a href="{{ route('owner.dashboard', $place->domain) }}" class="nav-link">
                                      <i class="fas fa-home nav-icon"></i>
                                      <p>{{ $place->name }}</p>
                                  </a>
                              </li>
                          @endforeach
                      </ul>
                  </li> --}}
                  <li class="nav-item">
                      <a href="{{ route('owner.dashboard', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain) ? 'active' : '' }}">
                          <i class="nav-icon fas fa-home"></i>
                          <p>
                              Home
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('owner.profile', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/profile') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-building"></i>
                          <p>
                              Profile Tempat
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('owner.dashboard', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/chat') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-comment"></i>
                          <p>
                              Chat
                          </p>
                      </a>
                  </li>
                  <li class="nav-header">Pengunjung</li>
                  <li class="nav-item">
                      <a href="{{ route('owner.barcode', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/barcode') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-barcode"></i>
                          <p>
                              Barcode Pengunjung
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('owner.exchange', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/exchange') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-gift"></i>
                          <p>
                              Tukar Point
                          </p>
                      </a>
                  </li>
                  <li class="nav-header">Produk</li>
                  <li class="nav-item">
                      <a href="{{ route('owner.product.tag', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/product/tag') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-tag"></i>
                          <p>
                              Etalase
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('owner.product', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/product') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-archive"></i>
                          <p>
                              Produk
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('owner.product.reward', $placeDomain) }}"
                          class="nav-link {{ Request::is('owner/' . $placeDomain . '/product/reward') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-cubes"></i>
                          <p>
                              Reward
                          </p>
                      </a>
                  </li>
              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
