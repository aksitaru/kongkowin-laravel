<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <div class="container-fluid">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
    
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('user.dashboard') }}" class="nav-link">Home</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="navbar-nav form-inline ml-3 w-100 justify-content-center">
            <div class="input-group w-50">
                <input class="form-control form-control-navbar" type="search" placeholder="Pencarian" aria-label="Pencarian">
                <div class="input-group-append">
                    <button class="input-group-text" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>


        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            {{-- <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Tentang Kongkowin</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Tempat</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Event</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Promo</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Blog</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Kontak Kami</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Premium</a>
            </li> --}}
            @guest

                <li class="nav-item d-flex align-items-center">
                    <button id="btn-login" class="btn btn-sm btn-outline-primary"> Masuk</button>
                </li>
                <li class="nav-item mx-2 d-flex align-items-center">
                    <a href="{{ route('user.register') }}" class="btn btn-sm btn-primary"> Daftar</a>
                </li>

            @else
                <li class="nav-item my-auto">
                    <a href="{{ route('user.myplace') }}" class="btn btn-sm btn-outline-primary"> Tempat</a>
                </li>

                <!-- Profile Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-user"></i>
                        <p class="d-none d-md-inline">{{ Auth::user()->name }}</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="dropdown-header">
                            <h6 class="font-weight-bold">{{ Auth::user()->name }}</h6>
                            <p>User</p>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a onclick="event.preventDefault();
                                    $('#logout-form').submit();" class="dropdown-item dropdown-footer">
                            <i class="fas fa-sign-out"></i>
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('user.auth.logout') }}" method="POST"
                            class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>

            @endguest
        </ul>
    </div>
</nav>
<!-- /.navbar -->
