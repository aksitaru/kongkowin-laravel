  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{ route('user.dashboard') }}" class="brand-link">
          <img src="{{ asset('images/logo-sidebar.png') }}" alt="Kongkowin"
              class="brand-image">
              <span class="brand-text">-</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- SidebarSearch Form -->
          {{-- <div class="form-inline">
              <div class="input-group" data-widget="sidebar-search">
                  <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                      aria-label="Search">
                  <div class="input-group-append">
                      <button class="btn btn-sidebar">
                          <i class="fas fa-search fa-fw"></i>
                      </button>
                  </div>
              </div>
          </div> --}}

          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                  <li class="nav-item">
                      <a href="/" class="nav-link {{ Request::is('/') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-home"></i>
                          <p>
                              Beranda
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="/place"
                          class="nav-link {{ Request::is('place') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-map-marker"></i>
                          <p>
                              Tempat
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="/admin/table/category"
                          class="nav-link {{ Request::is('admin/table/category') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-shopping-bag"></i>
                          <p>
                              Produk
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="/admin/table/facility"
                          class="nav-link {{ Request::is('admin/table/facility') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-calendar-alt"></i>
                          <p>
                              Acara
                          </p>
                      </a>
                  </li>
                  <li class="nav-header">
                      <hr>
                  </li>
                  <li class="nav-item">
                      <a href="/admin/table/facility"
                          class="nav-link {{ Request::is('admin/table/facility') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-pencil-alt"></i>
                          <p>
                              Blog
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/table/facility"
                        class="nav-link {{ Request::is('admin/table/facility') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-question-circle"></i>
                        <p>
                            Bantuan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/table/facility"
                        class="nav-link {{ Request::is('admin/table/facility') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-info-circle"></i>
                        <p>
                            Kirim Masukan
                        </p>
                    </a>
                </li>
                  <li class="nav-item">
                      <a href="/admin/table/user"
                          class="nav-link {{ Request::is('admin/table/user') ? 'active' : '' }}">
                          <i class="nav-icon fas fa-users"></i>
                          <p>
                              Tentang Kongkowin
                          </p>
                      </a>
                  </li>
              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
