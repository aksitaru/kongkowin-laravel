<div class="modal fade" id="modal-login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Masuk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('user.auth.login') }}" method="POST" data-modal='#modal-login'>
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username">Username atau Email</label>
                        <input id="username" class="form-control" name="username" required>
                        <small class="form-text text-muted">Contoh: {{ 'email@'. request()->getHost() }}</small>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <label for="password">Kata Sandi</label>
                            <a href="{{ route('user.dashboard') }}">Lupa kata sandi</a>    
                        </div>
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                </div>
            </form>
            <div class="modal-footer">
                <p class="text-muted">Belum punya akun? <a href="{{ route('user.register') }}"> Daftar</a></p>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->