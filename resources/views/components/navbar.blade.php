<nav class="navbar navbar-expand navbar-light bg-light px-3 p-2 mb-3">
    <div class="container-fluid">
        <div class="d-lg-flex w-100">
            <div class="d-flex w-100">
                <a href="{{ url('/') }}" class="navbar-brand d-lg-none">
                    <img src="https://getbootstrap.com/docs/4.6/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                </a>
                <a href="{{ url('/') }}" class="navbar-brand d-none d-lg-block">
                    <img src="https://getbootstrap.com/docs/4.6/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
                    {{ config('app.name', 'Kongkow') }}
                </a>
                <form class="w-100 navbar-search">
                    <input type="text" class="form-control navbar-search-input" placeholder="Cari Tempat, Acara, Makanan">
                    <button class="btn btn-sm navbar-search-button">
                        <span class="material-icons-round">
                            search
                        </span>
                    </button>
                </form>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">Tempat</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Acara</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Makanan</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Artikel</a>
                </li>
            </ul>
        </div>
        <div class="px-5 d-none d-lg-block">
            <ul class="navbar-nav">
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('myplace') }}" >Tempat</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownAkun" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>
                        <ul class="dropdown-menu dropdown-right-0" aria-labelledby="navbarDropdownAkun">
                            <li><a class="dropdown-item" href="#">Akun</a></li>
                            <li><a class="dropdown-item" href="{{ route('myplace') }}">Tempat Anda</a></li>
                            <li><a class="dropdown-item" href="#">Acara Anda</a></li>
                            <li><a class="dropdown-item" href="#">Riwayat</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li>
                                <a class="dropdown-item" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('user.auth.logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand navbar-light bg-light p-0 fixed-bottom d-lg-none">
    <ul class="navbar-nav w-100">
        <li class="nav-item flex-fill text-center">
            <a href="#" class="nav-link p-1">
                <span class="material-icons-round d-block">
                    home
                </span>
                Home
            </a>
        </li>
        <li class="nav-item flex-fill text-center">
            <a href="#" class="nav-link p-1">
                <span class="material-icons-round d-block">
                    dynamic_feed
                </span>
                Feed
            </a>
        </li>
        <li class="nav-item flex-fill text-center">
            <a href="#" class="nav-link p-1">
                <span class="material-icons-round d-block">
                    person
                </span>
                Akun
            </a>
        </li>
    </ul>
</nav>