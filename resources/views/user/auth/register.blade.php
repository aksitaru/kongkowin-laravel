@extends('layouts.user.auth')

@push('scripts')
    <script>
        $(function() {
            $('input[name=birthday]').daterangepicker({
                autoUpdateInput: true,
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1941,
                maxYear: parseInt(moment().format('YYYY'), 10) - 12,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });
    </script>
@endpush

@section('content')
    <div class="register-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-body">
                <div class="text-center">
                    <a href="/" class="h2">Daftar Sekarang</a>
                </div>
                <p class="register-box-msg">Sudah punya akun {{ config('app.name', 'Kongkow') }}? <a
                        href="{{ route('user.auth.register') }}">Masuk</a></p>

                <form action="{{ route('user.auth.register') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input id="username" name="username" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input id="name" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" name="email" class="form-control" required>
                        <small class="form-text text-muted">Contoh: {{ 'email@'. request()->getHost() }}</small>
                    </div>
                    <div class="form-group">
                        <label for="phone">Nomer HP</label>
                        <input id="phone" type="number" name="phone" class="form-control" required>
                        <small class="form-text text-muted">Contoh: 085XXXXXXXX</small>
                    </div>
                    <div class="form-group">
                        <label for="birthday">Tanggal Lahir</label>
                        <div class="input-group">
                            <input id="birthday" name="birthday" class="form-control" autocomplete="off" required>
                            <div class="input-group-append">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <input id="address" name="address" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Kata Sandi</label>
                        <input id="password" type="password" name="password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="password-confirm">Ulangi Kata Sandi</label>
                        <input id="password-confirm" type="password" name="password_confirmation" class="form-control"
                            required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endsection
