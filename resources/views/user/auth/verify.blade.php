@extends('layouts.admin.auth')

@push('scripts')
@endpush

@section('content')
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-body">
                <p class="login-box-msg">Silahkan cek email anda kami telah mengirimkan kode verifikasi akun anda lalu masukkan kode dibawah ini</p>
                <form action="{{ route('user.auth.verify') }}" method="post">
                    @csrf
                    <div class="input-group mb-3">
                        <input name="code" class="form-control" placeholder="Kode Verifikasi" required>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <button type="submit" class="btn btn-primary btn-block">Verifikasi</button>
                        </div>
                        <!-- /.col -->
                        <div class="col-5">
                            <a href="{{ route('user.auth.resend_code') }}" class="btn btn-outline-primary btn-block">Resend Code</a>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endsection
