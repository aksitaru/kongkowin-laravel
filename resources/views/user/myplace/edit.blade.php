@extends('layouts.app')

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select-multiple').select2({
                theme: 'bootstrap4'
            });
        });
    </script>

    <script>
        showAlert = function(message) {
            Swal.fire({
                title: message,
                html: '<input id="swal-input1" class="swal2-input mb-5">' +
                    '<input type="file" id="swal-input2" class="swal2-input">',
                focusConfirm: false,
                preConfirm: () => {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                    })
                }
            })
        }
    </script>
    @if (Session::has('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ Session::get('success') }}'
            })
        </script>
    @endif
@endpush

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/myplace">Tempat Saya</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $place->name }}</li>
        </ol>
    </nav>

    <h2 class="my-3">{{ $place->name }}</h2>

    <div class="row">
        <div class="col-lg-3">
            <x-side-nav.my-place-show placeDomain="{{ $place->domain }}" />
        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Ubah Tempat</h5>

                    <form method="POST" action="{{ route('myplace.edit', $place) }}">
                        @method('put')
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama Tempat</label>

                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') ?: $place->name }}" required autocomplete="name"
                                autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="domain">Domain Tempat</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon3">{{ request()->getHost() }}/</span>
                            </div>
                            <input id="domain" type="text" class="form-control @error('domain') is-invalid @enderror"
                                name="domain" value="{{ old('domain') ?: $place->domain }}" required autocomplete="domain"
                                autofocus>

                            @error('domain')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="categories">Kategori</label>
                            <select id="categories"
                                class="form-control select-multiple @error('address') is-invalid @enderror"
                                name="categories[]" multiple>
                                @foreach ($p_cat as $item)
                                    <option value="{{ $item->id }}"
                                        {{ in_array($item->id, old('categories') ?: $place->categories->pluck('category_id')->toArray()) ? 'selected' : '' }}>
                                        {{ $item->name }}</option>
                                @endforeach
                            </select>

                            @error('categories')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <div class="form-group">
                            <label for="address">Alamat Tempat</label>

                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror"
                                name="address" value="{{ old('address') ?: $place->address }}" required
                                autocomplete="address" autofocus>

                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">Deskripsi Tempat</label>

                            <textarea id="description" rows='3' type="text"
                                class="form-control @error('description') is-invalid @enderror" name="description" required
                                autocomplete="description"
                                autofocus>{{ old('description') ?: $place->description }}</textarea>

                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="facilities">Fasilitas</label>
                            <select id="facilities"
                                class="form-control select-multiple @error('address') is-invalid @enderror"
                                name="facilities[]" multiple>
                                @foreach ($p_fac as $item)
                                    <option value="{{ $item->id }}"
                                        {{ in_array($item->id, old('facilities') ?: $place->facilities->pluck('facility_id')->toArray()) ? 'selected' : '' }}>
                                        {{ $item->name }}</option>
                                @endforeach
                            </select>

                            @error('facilities')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <button type="submit" class="btn btn-primary">Ubah</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
