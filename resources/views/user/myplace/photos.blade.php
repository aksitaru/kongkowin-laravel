@extends('layouts.app')

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select-multiple').select2({
                theme: 'bootstrap4'
            });
        });
    </script>

    <script>
        $('#add-photo').click(function() {
            $('#photo-modal-label').html("Tambah Foto")

            $('#photo-modal').modal('show')
        })

        $('#form-add-photo').submit(function(event) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var url = '{{ route('myplace.add_photo', $place) }}';

            $.ajax({
                type: "POST",
                url: url,
                data: new FormData(this),
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function(data) {
                    Swal.fire(
                        'Loading',
                        'Mohon Tunggu',
                        ''
                    )
                    Swal.showLoading()
                },
                success: function(data) {
                    if (data.result) {
                        Swal.fire(
                            data.message.head,
                            data.message.body,
                            'success'
                        )
                        location.href = data.redirect;
                    } else {
                        Swal.fire(
                            data.message.head,
                            data.message.body,
                            'error'
                        )
                    }
                },
                error: function(data) {
                    console.log(data);
                }
            });
            event.preventDefault();
        })

        showAlert = function(message) {
            Swal.fire({
                title: message,
                html: '<input id="swal-input1" class="swal2-input m-0">' +
                    '<input type="file" id="swal-input2" class="swal2-file">',
                focusConfirm: false,
                preConfirm: () => {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                    })
                }
            })
        }
    </script>
    @if (Session::has('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ Session::get('success') }}'
            })
        </script>
    @endif
@endpush

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="photo-modal" tabindex="-1" aria-labelledby="photo-modal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="photo-modal-label">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-add-photo">
                        @csrf
                        <div class="form-group">
                            <label for="photo-modal-input-name">Name</label>
                            <input name="name" type="name" class="form-control" id="photo-modal-input-name"
                                placeholder="Contoh: View Depan Lokasi" required>
                        </div>

                        <div class="form-group">
                            <label for="photo-modal-input-file-photo">File Foto</label>
                            <input name="file" type="file" class="form-control form-control-file"
                                id="photo-modal-input-file-photo" accept="image/*" required>
                        </div>
                        <button type="submit" type="button" class="btn btn-primary">Tambah Foto</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/myplace">Tempat Saya</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $place->name }}</li>
        </ol>
    </nav>

    <h2 class="my-3">{{ $place->name }}</h2>

    <div class="row">
        <div class="col-lg-3">
            <x-side-nav.my-place-show placeDomain="{{ $place->domain }}" />
        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Foto Tempat</h5>

                    <button id="add-photo" class="btn btn-sm btn-primary">Tambah Foto</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
