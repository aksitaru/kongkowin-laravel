@extends('layouts.user.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="d-flex justify-content-beetween">
                    <h2>Tempat Anda</h2>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <a href="{{ route('user.myplace.create') }}" class="btn btn-primary mb-2">Tambah Tempat</a>
                <div class="row row-cols-1 row-cols-md-3">
                    @foreach ($places as $place)
                        @php
                            $images = $place->images()->limit(1)->get()->pluck('file_path')->toArray();
                        @endphp
                        <div class="col">
                            <a href="{{ route('owner.dashboard', $place->domain) }}">
                                <div class="card">
                                    @if (count($images) > 0)
                                        <img src="/storage/{{ $images[0] }}"
                                            class="card-img-top img-main-place-dashboard" alt="...">
                                    @else
                                        <img src="{{ asset('images/empty.jpg') }}"
                                            class="card-img-top img-main-place-dashboard" alt="...">
                                    @endif
                                    <div class="card-body">
                                        <div class="media">
                                            @if ($place->logo_path != null)
                                                <img class="img-fluid img-circle img-60 mr-2"
                                                    src="/storage/{{ $place->logo_path }}">
                                            @else
                                                <img class="img-fluid img-circle img-60 mr-2"
                                                    src="{{ asset('images/empty.jpg') }}">
                                            @endif
                                            <div class="media-body ml-2">
                                                <div class="text-bold text-lg">
                                                    {{ $place->name }}
                                                </div>

                                                <div class="star">
                                                    @php
                                                        $rates = $place->rates;
                                                        $rating_avg = number_format($rates->avg('rating'), 1);
                                                        $rating_count = $rates->count();
                                                        
                                                        if ($rating_avg < 1.5) {
                                                            $btnClass = 'btn-danger';
                                                        } elseif ($rating_avg < 3.5) {
                                                            $btnClass = 'btn-warning';
                                                        } else {
                                                            $btnClass = 'btn-success';
                                                        }
                                                        
                                                    @endphp
                                                    <button class="btn btn-xs {{ $btnClass }}">
                                                        {{ $rating_avg }}
                                                        <span class="fas fa-star fa-xs"></span>
                                                    </button>
                                                    <span class="text-muted ml-2">{{ $rating_count }} Ulasan</span>
                                                </div>
                                            </div>
                                        </div>

                                        <p class="text-muted mt-2">
                                            @foreach ($place->categories as $category)
                                                <span class="badge badge-primary">{{ $category->name }}</span>
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <!-- /.card -->
                        </div>
                    @endforeach
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{-- <div class="row pt-3">
    <div class="col-lg-9">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Tempat Anda</h5>
                <div class="card-deck">
                    @foreach ($places as $place)
                        <a href="myplace/{{ $place->domain }}" class="text-decoration-none">
                            <div class="card dashboard-card-place">
                                <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg"
                                    class="card-img-top" alt="{{ $place->name }}">
                                <div class="card-body p-3">
                                    <h5 class="card-title">{{ $place->name }}</h5>
                                    <div class="place-categories mb-2">
                                        <span class="badge badge-pill badge-primary">Rumah Makan</span>
                                    </div>
                                    <div class="row mx-0 star-rating align-items-center">
                                        <span class="material-icons-round">
                                            star
                                        </span>
                                        <span class="material-icons-round">
                                            star
                                        </span>
                                        <span class="material-icons-round">
                                            star
                                        </span>
                                        <span class="material-icons-round">
                                            star
                                        </span>
                                        <span class="material-icons-round">
                                            star_outline
                                        </span>
                                        <p class="d-inline ml-2 m-0 text-muted">4 (200)</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
          </div>
    </div>
</div> --}}
@endsection
