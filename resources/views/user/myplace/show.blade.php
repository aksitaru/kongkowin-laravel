@extends('layouts.app')

@push('scripts')
    <script>
        showAlert = function(message)
        {
            Swal.fire({
                title: message,
                html:
                    '<input id="swal-input1" class="swal2-input mb-5">' +
                    '<input type="file" id="swal-input2" class="swal2-input">',
                focusConfirm: false,
                preConfirm: () => {
                    Swal.fire({
                        icon: "success",
                        title: "Sukses",
                    })
                }
            })
        }
    </script>
@endpush

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/myplace">Tempat Saya</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $place->name }}</li>
        </ol>
    </nav>
    
    <h2 class="my-3">{{ $place->name }}</h2>
        
    <div class="row">
        <div class="col-lg-3">
            <x-side-nav.my-place-show placeDomain="{{ $place->domain }}"/>
        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dashboard</h5>
        
                    <p class="mb-2">Place Photos</p>
                    <div class="card mb-3">
                        <div class="card-body">
                            <button onclick="showAlert('test')" class="btn btn-sm btn-primary">Tambah Foto</button>
                        </div>
                    </div>
        
                    <p class="mb-2">Produk Photos</p>
                    <div class="card">
                        <div class="card-body">
                            <button class="btn btn-sm btn-primary">Tambah Menu</button>                    
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div>
@endsection