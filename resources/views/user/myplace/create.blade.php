@extends('layouts.user.app')

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select-multiple').select2({
                theme: 'bootstrap4'
            });
        });
    </script>
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row full-page align-items-center">
                    <div class="col-md-4 text-center">
                        <h2>Tambahkan Tempat Anda disini, dapat memperluas pasar anda!</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title text-lg">Halo <b>{{ Auth::user()->name }}</b>, ayo isi detail
                                    tempatmu!</h5>
                            </div>
                            <div class="card-body">

                                <form method="POST" action="{{ route('user.myplace.create') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Nama Tempat</label>
                                        <input id="name" type="text" class="form-control" name="name" autofocus>
                                    </div>

                                    <label for="domain">Domain Tempat</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"
                                                id="basic-addon3">{{ request()->getHost() }}/</span>
                                        </div>
                                        <input id="domain" type="text" class="form-control" name="domain">
                                    </div>

                                    <div class="form-group">
                                        <label for="categories">Kategori</label>
                                        <select id="categories" class="form-control select-multiple" name="categories[]"
                                            multiple>
                                            @foreach ($p_cat as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Alamat Tempat</label>

                                        <input id="address" type="text" class="form-control" name="address">
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Deskripsi Tempat</label>

                                        <textarea id="description" rows='3' type="text" class="form-control"
                                            name="description"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="facilities">Fasilitas</label>
                                        <select id="facilities" class="form-control select-multiple" name="facilities[]"
                                            multiple>
                                            @foreach ($p_fac as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Lanjut</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
