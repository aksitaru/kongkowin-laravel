{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.user.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="d-flex justify-content-beetween">
                    <h2>Tempat Pilihan</h2>
                    <a class="ml-auto my-auto" href="place">Lihat Semua</a>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    @foreach ($places as $place)
                        @php
                            $images = $place->images()->limit(5)->get()->pluck('file_path')->toArray();
                        @endphp
                        <div class="col-sm-1 col-md-auto">
                            <a href="{{ route('place.show', $place) }}">
                                <div class="card">
                                    @if (count($images) > 0)
                                        <img src="/storage/{{ $images[0] }}"
                                            class="card-img-top img-main-place-dashboard" alt="...">
                                    @else
                                        <img src="{{ asset('images/empty.jpg') }}"
                                            class="card-img-top img-main-place-dashboard" alt="...">
                                    @endif
                                    <div class="card-body">
                                        <div class="media">
                                            @if ($place->logo_path != null)
                                                    <img class="img-fluid img-circle img-60 mr-2"
                                                        src="/storage/{{ $place->logo_path }}">
                                                @else
                                                    <img class="img-fluid img-circle img-60 mr-2"
                                                        src="{{ asset('images/empty.jpg') }}">
                                                @endif
                                            <div class="media-body ml-2">
                                                <div class="text-bold text-lg">
                                                    {{ $place->name }}
                                                </div>

                                                <div class="star">
                                                    @php
                                                        $rates = $place->rates;
                                                        $rating_avg = number_format($rates->avg('rating'), 1);
                                                        $rating_count = $rates->count();
                                                        
                                                        if ($rating_avg < 1.5) {
                                                            $btnClass = 'btn-danger';
                                                        } elseif ($rating_avg < 3.5) {
                                                            $btnClass = 'btn-warning';
                                                        } else {
                                                            $btnClass = 'btn-success';
                                                        }
                                                        
                                                    @endphp
                                                    <button class="btn btn-xs {{ $btnClass }}">
                                                        {{ $rating_avg }}
                                                        <span class="fas fa-star fa-xs"></span>
                                                    </button>
                                                    <span class="text-muted ml-2">{{ $rating_count }} Ulasan</span>
                                                </div>
                                            </div>
                                        </div>

                                        <p class="text-muted mt-2">
                                            @foreach ($place->categories as $category)
                                                <span
                                                    class="badge badge-primary">{{ $category->name }}</span>
                                            @endforeach
                                        </p>        

                                        <hr>

                                        <div class="row">
                                            @for ($i = 0; $i < 4; $i++)
                                                <div class="col-3 p-1">
                                                    @if (count($images) > $i + 1)
                                                        <img src="/storage/{{ $images[$i] }}"
                                                            class="img-thumbnail rounded p-0 img-80" alt="...">
                                                    @else
                                                        <img src="{{ asset('images/empty.jpg') }}"
                                                            class="img-thumbnail rounded p-0 img-80" alt="...">
                                                    @endif
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!-- /.card -->
                        </div>
                    @endforeach
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
