{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.admin.app')

@push('scripts')
    <script type="text/javascript">
        $(function() {
            var table = $('#table1').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('admin.table.user.list') }}",
                    type: "POST",
                    data: function(d) {
                        d._token = '{{ csrf_token() }}'
                    },
                },
                order: [],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'username',
                        name: 'username'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'places_count',
                        name: 'places_count'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
        });

        $('#btn-add-row').click(function() {
            $('#modal-add-row').find('form').attr('action', '{{ url('admin/table/user/add') }}');
            $('#modal-add-row').find('.modal-title').html('Tambah Pengguna');
            $('#modal-add-row').modal('show');
        });

        $('body').on('click', '#btn-edit-row', function() {
            var id = $(this).data('id');

            var url = '{{ url('admin/table/user/show') }}/' + id;

            var data = new FormData();
            data.append('_token', '{{ csrf_token() }}');

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                beforeSend: function(data) {
                    Swal.fire("Loading", "Mohon Tunggu", "");
                    Swal.showLoading();
                },
                success: function(data) {
                    Swal.close();

                    $('#modal-edit-row').find('form').attr('action',
                        '{{ url('admin/table/user/update') }}/' + id);
                    $('#modal-edit-row').find('.modal-title').html('Edit ' + data.name);
                    $('#modal-edit-row').find('input[name=name]').val(data.name);
                    $('#modal-edit-row').modal('show');
                },
                error: function(data) {
                    Swal.fire("Error Request", "Coba beberapa saat lagi.", "error");
                },
            });
        });

        $('body').on('click', '#btn-delete-row', function() {
            var id = $(this).data('id');

            var url = '{{ url('admin/table/user/show') }}/' + id;

            var data = new FormData();
            data.append('_token', '{{ csrf_token() }}');

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                beforeSend: function(data) {
                    Swal.fire("Loading", "Mohon Tunggu", "");
                    Swal.showLoading();
                },
                success: function(data) {
                    Swal.close();

                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success ml-3',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        reverseButtons: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#delete-row').attr('action', '{{ url('admin/table/user/delete') }}/' + id);

                            $('#delete-row').submit();
                        }
                    })
                },
                error: function(data) {
                    Swal.fire("Error Request", "Coba beberapa saat lagi.", "error");
                },
            });
        });
    </script>
@endpush
@section('content')
    <div class="modal fade" id="modal-edit-row">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit ...</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" data-modal='#modal-edit-row' data-table='#table1'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <form id="delete-row" method="POST" class="d-none" data-table="#table1">@csrf</form>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Tabel Pengguna</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">Pengguna</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Username</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Total Tempat</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
