@extends('layouts.app')

@section('content')
  <div id="place-section" class="py-3">
      <div class="row align-items-end px-3">
          <h2 class="font-weight-bolder flex-fill flex-lg-fill-unset">Tempat</h2>
          <a href="place" class="px-2 font-weight-bolder h5">Lihat Semua</a>
      </div>
      <div class="row d-block horizontal-scroll">
        <div class="px-3">
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
          <a href="{{ url('/place/dapur-ngebul') }}">
            <div class="card d-inline-block" style="width: 14rem;">
              <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
              <div class="card-body p-2">
                <p class="card-text font-weight-bolder">Dapur Ngebul</p>
                <p class="card-text">Tegal</p>
              </div>
            </div>
          </a>
        </div>
      </div>
  </div>

  <div id="food-section" class="py-3">
    <div class="row align-items-end px-3">
        <h2 class="font-weight-bolder flex-fill flex-lg-fill-unset">Tempat</h2>
        <a href="place" class="px-2 font-weight-bolder h5">Lihat Semua</a>
    </div>
    <div class="row d-block horizontal-scroll">
      <div class="px-3">
        <div class="card d-inline-block" style="width: 14rem;">
          <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
          <div class="card-body p-2">
            <h5 class="card-title">Dapur Ngebul</h5>
            <p class="card-text">Tegal</p>
          </div>
        </div>
        <div class="card d-inline-block" style="width: 14rem;">
            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
            <div class="card-body p-2">
              <h5 class="card-title">Dapur Ngebul</h5>
              <p class="card-text">Tegal</p>
            </div>
        </div>
        <div class="card d-inline-block" style="width: 14rem;">
            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
            <div class="card-body p-2">
              <h5 class="card-title">Dapur Ngebul</h5>
              <p class="card-text">Tegal</p>
            </div>
        </div>
        <div class="card d-inline-block" style="width: 14rem;">
            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
            <div class="card-body p-2">
              <h5 class="card-title">Dapur Ngebul</h5>
              <p class="card-text">Tegal</p>
            </div>
        </div>
        <div class="card d-inline-block" style="width: 14rem;">
            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
            <div class="card-body p-2">
              <h5 class="card-title">Dapur Ngebul</h5>
              <p class="card-text">Tegal</p>
            </div>
        </div>
        <div class="card d-inline-block" style="width: 14rem;">
            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg?impolicy=fcrop&w=1000&h=666&q=medium" class="card-img-top" alt="Dapur Ngebul">
            <div class="card-body p-2">
              <h5 class="card-title">Dapur Ngebul</h5>
              <p class="card-text">Tegal</p>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
