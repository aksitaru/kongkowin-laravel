{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.user.app')

@push('scripts')

    {{-- Script need auth:user --}}
    <script>
        $(function() {
            $('.btn-rate').click(function(e) {
                e.preventDefault();

                var url = '{{ route('place.add_rate', $place) }}';

                var btn = $(this);
                var rating = btn.data('rate');

                var data = new FormData();
                data.append('_token', '{{ csrf_token() }}');
                data.append('rating', rating);

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    beforeSend: function(data) {
                        Swal.fire("Loading", "Mohon Tunggu", "");
                        Swal.showLoading();
                    },
                    success: function(data) {
                        try {
                            Swal.close();
                            if (data.result == true) {
                                $('.btn-rate').removeClass('btn-success');
                                $('.btn-rate').removeClass('btn-warning');
                                $('.btn-rate').removeClass('btn-danger');
                                $('.btn-rate').addClass('btn-default');

                                btn.removeClass('btn-default');
                                switch (rating) {
                                    case 1:
                                        btn.addClass('btn-danger');
                                        break;

                                    case 2:
                                        btn.addClass('btn-warning');
                                        break;

                                    case 3:
                                        btn.addClass('btn-warning');
                                        break;

                                    case 4:
                                        btn.addClass('btn-success');
                                        break;

                                    case 5:
                                        btn.addClass('btn-success');
                                        break;

                                    default:
                                        btn.addClass('btn-defa=ult');
                                        break;
                                }
                            }
                        } catch (error) {
                            Swal.fire("Error Request", error.toString(), "error");
                        }
                    },
                    error: function(request) {
                        try {
                            Swal.close();

                            data = request.responseJSON;
                            if (data.result == false) {
                                $('#modal-login').modal('show');
                                if (data.data.login == false) {
                                    $('#modal-login').modal('show');
                                }
                            }
                        } catch (error) {
                            Swal.fire("Error Request", error.toString(), "error");
                        }
                    },
                });

            });

            $('#new-review').click(function(e) {
                e.preventDefault();
                $('#modal-add-review').modal('show');
            });
        });
    </script>

    {{-- Script to Tab --}}
    <script>
        function scrollToCardTabs() {
            $('html, body').animate({
                scrollTop: $(".card-tabs").offset().top
            }, 700);
        }

        function addHasUrl(name) {
            if (name == '') {
                parent.location.hash = "/";
            } else {
                parent.location.hash = "/" + name + "/";
            }
        }

        function connectTab(nameUrl, selectorElementTab, selectorTriggerOpen) {
            if (window.location.href.indexOf('#/' + nameUrl + '/') != -1) {
                $(selectorElementTab).click();

                scrollToCardTabs();
            }

            $(selectorTriggerOpen).click(function(e) {
                e.preventDefault();
                $(selectorElementTab).click();

                scrollToCardTabs();
                addHasUrl(nameUrl);
            });
        }

        // photo tab
        var nameUrlPhotosTab = 'photo';
        var selectorElementPhotosTab = '#place-photos-tab';
        var selectorTriggerOpenPhotosTab = "#show-all-photos";

        connectTab(nameUrlPhotosTab, selectorElementPhotosTab, selectorTriggerOpenPhotosTab);

        // product tab
        var nameUrlProductsTab = 'product';
        var selectorElementProductsTab = '#place-products-tab';
        var selectorTriggerOpenProductsTab = "#show-all-products";

        connectTab(nameUrlProductsTab, selectorElementProductsTab, selectorTriggerOpenProductsTab);

        // review tab
        var nameUrlReviewTab = 'review';
        var selectorElementReviewTab = '#place-reviewtab-tab';
        var selectorTriggerOpenReviewTab = ".show-all-reviews";
        connectTab(nameUrlReviewTab, selectorElementReviewTab, selectorTriggerOpenReviewTab);
    </script>

    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
@endpush

@section('content')

    <div class="modal fade" id="modal-add-review">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Ulasan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" data-modal='#modal-add-review' data-table='#table1'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="card card-solid">
                    <div class="card-body">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>        
                    </div>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid pb-4">
                <div class="row">
                    <div class="col-xl-7">
                        <!-- Default box -->
                        <div class="card card-solid">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-7">
                                        @php
                                            $images = $place
                                                ->images()
                                                ->limit(5)
                                                ->pluck('file_path')
                                                ->toArray();
                                            
                                            $image_empty = asset('images/empty.jpg');
                                            $image1 = count($images) > 0 ? 'storage/' . $images[0] : $image_empty;
                                            $image2 = count($images) > 1 ? 'storage/' . $images[1] : $image_empty;
                                            $image3 = count($images) > 2 ? 'storage/' . $images[2] : $image_empty;
                                            $image4 = count($images) > 3 ? 'storage/' . $images[3] : $image_empty;
                                            $image5 = count($images) > 4 ? 'storage/' . $images[4] : $image_empty;
                                        @endphp
                                        <img src="{{ $image1 }}"
                                            class="img-thumbnail rounded p-0 img-place-show-main">
                                        {{-- <img src="{{ asset('images/empty.jpg') }}" class="img-thumbnail rounded img-place-show-main"> --}}
                                    </div>
                                    <div class="col-12 col-sm-5 my-sm-n1">
                                        <div class="row row-cols-4 row-cols-sm-2 pt-2 pt-sm-0">
                                            <div class="col py-sm-1 px-sm-1">
                                                <img class="img-thumbnail rounded p-0 img-place-show-other"
                                                    src="{{ $image2 }}">
                                            </div>
                                            <div class="col py-sm-1 px-sm-1">
                                                <img class="img-thumbnail rounded p-0 img-place-show-other"
                                                    src="{{ $image3 }}">
                                            </div>
                                            <div class="col py-sm-1 px-sm-1">
                                                <img class="img-thumbnail rounded p-0 img-place-show-other"
                                                    src="{{ $image4 }}">
                                            </div>
                                            <div class="col py-sm-1 px-sm-1">
                                                <a id="show-all-photos" href="#/photos/">
                                                    <div class="image-content" data-content="20+">
                                                        <img class="img-thumbnail rounded p-0 img-place-show-other"
                                                            src="{{ $image5 }}">
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id="place-title">
                                    <div class="row my-2">
                                        <h3 class="col-12 col-md-8">{{ $place->name }}</h3>
                                        <a href="#" class="col-12 col-md-4 mb-auto text-md-right show-all-reviews">
                                            @php
                                                $rates = $place->rates;
                                                $rating_avg = number_format($rates->avg('rating'), 1);
                                                $rating_count = $rates->count();
                                                
                                                if ($rating_avg < 1.5) {
                                                    $btnClass = 'btn-danger';
                                                } elseif ($rating_avg < 3.5) {
                                                    $btnClass = 'btn-warning';
                                                } else {
                                                    $btnClass = 'btn-success';
                                                }
                                                
                                            @endphp
                                            <button class="btn btn-sm {{ $btnClass }}">
                                                {{ $rating_avg }}
                                                <span class="fas fa-star fa-xs"></span>
                                            </button>
                                            <span class="text-muted ml-2">{{ $rating_count }} Ulasan</span>
                                        </a>
                                    </div>
                                    <p class="text-muted">
                                        <span>{{ $place->categories->implode('name', ', ') }}</span>
                                    <div id="place-button-shortcut">
                                        <button class="btn btn-primary">
                                            <span class="far fa-star"></span>
                                            Tambah Ulasan
                                        </button>
                                        <button class="btn btn-outline-primary">
                                            <span class="far fa-map"></span>
                                            Petunjuk
                                        </button>
                                        <button class="btn btn-outline-primary">
                                            <span class="far fa-bookmark"></span>
                                            Simpan
                                        </button>
                                        <button class="btn btn-outline-primary">
                                            <span class="fas fa-share-alt"></span>
                                            Bagikan
                                        </button>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="place-tab" role="tablist">
                                    <li class="nav-item" onclick="addHasUrl('')"><a class="nav-link active"
                                            id="place-desc-tab" data-toggle="tab" href="#place-desc" role="tab"
                                            aria-controls="place-desc" aria-selected="true"><span
                                                class="fas fa-home d-md-none"></span><span
                                                class="d-none d-md-block">Sekilas</span></a></li>
                                    <li class="nav-item" onclick="addHasUrl('photo')"><a class="nav-link"
                                            id="place-photos-tab" data-toggle="tab" href="#place-photos" role="tab"
                                            aria-controls="place-photos" aria-selected="false"><span
                                                class="fas fa-camera d-md-none"></span><span
                                                class="d-none d-md-block">Foto</span></a></li>
                                    <li class="nav-item" onclick="addHasUrl('product')"><a class="nav-link"
                                            id="place-products-tab" data-toggle="tab" href="#place-products" role="tab"
                                            aria-controls="place-products" aria-selected="false"><span
                                                class="fas fa-box d-md-none"></span><span
                                                class="d-none d-md-block">Produk</span></a></li>
                                    <li class="nav-item" onclick="addHasUrl('review')"><a class="nav-link"
                                            id="place-reviewtab-tab" data-toggle="tab" href="#place-reviewtab" role="tab"
                                            aria-controls="place-reviewtab" aria-selected="false"><span
                                                class="fas fa-star d-md-none"></span><span
                                                class="d-none d-md-block">Ulasan</span></a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="col-12">
                                    <nav class="w-100">
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="place-desc" role="tabpanel"
                                            aria-labelledby="place-desc-tab">
                                            <div class="row">
                                                <div class="col-12 col-md-8 mb-4 mb-md-3 px-0 pr-md-2">
                                                    <div id="place-description">
                                                        <h4>Tentang tempat ini</h4>
                                                        <p>
                                                            {{ $place->description }}
                                                        </p>
                                                    </div>

                                                    <div id="place-facilities">
                                                        <h4>Fasilitas</h4>
                                                        <p class="text-muted">
                                                            @foreach ($place->facilities as $facility)
                                                                <span
                                                                    class="badge badge-primary">{{ $facility->name }}</span>
                                                            @endforeach
                                                        </p>
                                                    </div>

                                                    {{-- <div id="place-tagss">
                                                <div class="d-flex justify-content-beetween">
                                                    <h4>Etalase Produk</h4>
                                                    <a id="show-all-tags" href="#place-tags" class="ml-auto my-auto">Lihat
                                                        semua etalase</a>
                                                </div>
                                                <p>
                                                    @foreach ($place->tags()->limit(6)->get()
    as $tag)
                                                        <button class="btn btn-default">
                                                            {{ $tag->name }}
                                                            <span
                                                                class="badge badge-primary">{{ $tag->products()->count() }}</span>
                                                        </button>
                                                    @endforeach
                                                </p>
                                            </div> --}}

                                                    <div id="place-productss">
                                                        <div class="d-flex justify-content-beetween">
                                                            <h4>Semua Produk</h4>
                                                            <a id="show-all-products" href="#place-products"
                                                                class="ml-auto my-auto">Lihat semua produk</a>
                                                        </div>

                                                        <div class="row row-cols-2 row-cols-md-3">
                                                            @foreach ($place->products()->limit(12)->get()
        as $product)
                                                                <div class="col">
                                                                    @php
                                                                        $product_image =
                                                                            $product->images()->count() > 0
                                                                                ? 'storage/' .
                                                                                    $product
                                                                                        ->images()
                                                                                        ->select('file_path')
                                                                                        ->limit(1)
                                                                                        ->get()
                                                                                        ->pluck('file_path')
                                                                                        ->toArray()[0]
                                                                                : asset('images/empty.jpg');
                                                                    @endphp
                                                                    <img class="img-thumbnail img-h-70"
                                                                        src="{{ $product_image }}"
                                                                        alt="{{ $product->name }}" loading="lazy">
                                                                    <h6 class="text-bold my-0">{{ $product->name }}</h6>
                                                                    <p>Rp. {{ $product->price }}</p>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <nav aria-label="Page navigation example">
                                                            <ul class="pagination justify-content-end">
                                                                <li class="page-item">
                                                                    <a class="page-link" href="#"><span
                                                                            class="fas fa-angle-right"></span></a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>

                                                    {{-- <div id="place-description">
                                                <h4>Harga rata-rata</h4>
                                                <p>
                                                    Rata-rata harga
                                                </p>
                                            </div> --}}

                                                </div>
                                                <div class="col-12 col-md-4 pl-md-3">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <h4>Telepon</h4>
                                                            <p>
                                                                +6287728864687
                                                                <button class="btn btn-xs btn-outline-primary">
                                                                    <span class="fas fa-clone"></span>
                                                                </button>
                                                            </p>

                                                            <h4>Petunjuk</h4>
                                                            iframe map
                                                            <p>
                                                                {{ $place->address }}
                                                                <button class="btn btn-xs btn-outline-primary">
                                                                    <span class="fas fa-clone"></span>
                                                                </button>
                                                            </p>
                                                            <button class="btn btn-outline-primary">
                                                                <span class="far fa-map"></span>
                                                                Petunjuk
                                                            </button>

                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div id="place-review">
                                                                <div id="place-review-media">
                                                                    <div class="media mb-2">
                                                                        <img src="{{ asset('images/empty.jpg') }}"
                                                                            class="mr-3 img-50" alt="Namanya">
                                                                        <div class="media-body">
                                                                            <h5 class="my-0">Namanya</h5>
                                                                            <p class="mb-0">100 Ulasan - 10
                                                                                Pengikut</p>
                                                                        </div>
                                                                    </div>
                                                                    <div id="place-review-media-rating"
                                                                        class="mb-1">
                                                                        <button class="btn btn-sm btn-success">
                                                                            5
                                                                            <span class="fas fa-star fa-xs"></span>
                                                                        </button>

                                                                        <span class="text-muted ml-1">1 bulan lalu</span>
                                                                    </div>
                                                                    <p>
                                                                        It's a really great moment while im enjoying my
                                                                        meals in one
                                                                        of the
                                                                        tallest buildings in Jakarta on 56th floor. The food
                                                                        is
                                                                        amazing ,
                                                                        fast service and so yummy..ive got happy tummy. Well
                                                                        done
                                                                        Skye. Love
                                                                        being here again.
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <a href="#place-reviewtab" class="show-all-reviews">Lihat semua
                                                                ulasan</a>

                                                            <hr>

                                                            <div id="place-review-new">
                                                                <h4>Beri nilai anda pada tempat ini</h4>
                                                                <p>
                                                                    @php
                                                                        $user_rating = null;
                                                                        if (Auth::check()) {
                                                                            $rate = Auth::user()
                                                                                ->rates()
                                                                                ->where('place_id', $place->id)
                                                                                ->get();
                                                                            if ($rate->count() == 0) {
                                                                                $classBtnRate = 'btn-default';
                                                                            } else {
                                                                                $user_rating = $rate[0]['rating'];
                                                                                switch ($user_rating) {
                                                                                    case 1:
                                                                                        $classBtnRate = 'btn-danger';
                                                                                        break;
                                                                        
                                                                                    case 2:
                                                                                        $classBtnRate = 'btn-warning';
                                                                                        break;
                                                                                    case 3:
                                                                                        $classBtnRate = 'btn-warning';
                                                                                        break;
                                                                                    case 4:
                                                                                        $classBtnRate = 'btn-success';
                                                                                        break;
                                                                                    case 5:
                                                                                        $classBtnRate = 'btn-success';
                                                                                        break;
                                                                                    default:
                                                                                        $classBtnRate = 'btn-default';
                                                                                        break;
                                                                                }
                                                                            }
                                                                        } else {
                                                                        }
                                                                    @endphp
                                                                    @for ($i = 1; $i <= 5; $i++)
                                                                        <button
                                                                            class="btn btn-sm {{ $i == $user_rating ? $classBtnRate : 'btn-default' }} btn-rate"
                                                                            data-rate="{{ $i }}">
                                                                            {{ $i }}
                                                                            <span class="fas fa-star fa-xs"></span>
                                                                        </button>
                                                                    @endfor
                                                                </p>
                                                                <p><a id="new-review" href="#">Tulis Ulasan</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="place-reviewtab" role="tabpanel"
                                            aria-labelledby="place-reviewtab-tab">
                                            <h4>Ulasan</h4>

                                            Vivamus rhoncus nisl sed venenatis luctus. Sed
                                            condimentum risus ut tortor feugiat laoreet. Suspendisse potenti. Donec et
                                            finibus sem,
                                            ut
                                        </div>
                                        <div class="tab-pane" id="place-photos" role="tabpanel"
                                            aria-labelledby="place-photos-tab">
                                            <h4 id="header-photo-tab">Foto</h4>

                                            <div class="row row-cols-2 row-cols-md-4">
                                                @foreach ($place->images()->limit(24)->get()
        as $image)
                                                    @php
                                                        $image_file_path = 'storage/' . $image->file_path;
                                                    @endphp
                                                    <a href="{{ $image_file_path }}" data-toggle="lightbox"
                                                        data-gallery="hidden-images" class="col mb-2">
                                                        <img class="img-thumbnail img-h-70" src="{{ $image_file_path }}"
                                                            loading="lazy">
                                                    </a>
                                                @endforeach
                                            </div>
                                            <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-end">
                                                    <li class="page-item">
                                                        <a class="page-link" href="#"><span
                                                                class="fas fa-angle-right"></span></a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div class="tab-pane" id="place-products" role="tabpanel"
                                            aria-labelledby="place-products-tab">
                                            <h4>Produk</h4>

                                            <div class="row row-cols-2 row-cols-md-4">
                                                @foreach ($place->products()->limit(24)->get()
        as $product)
                                                    <div class="col">
                                                        @php
                                                            $product_image =
                                                                $product->images()->count() > 0
                                                                    ? 'storage/' .
                                                                        $product
                                                                            ->images()
                                                                            ->select('file_path')
                                                                            ->limit(1)
                                                                            ->get()
                                                                            ->pluck('file_path')
                                                                            ->toArray()[0]
                                                                    : asset('images/empty.jpg');
                                                        @endphp
                                                        <img class="img-thumbnail img-h-70" src="{{ $product_image }}"
                                                            alt="{{ $product->name }}" loading="lazy">
                                                        <h6 class="text-bold my-0">{{ $product->name }}</h6>
                                                        <p>Rp. {{ $product->price }}</p>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-end">
                                                    <li class="page-item">
                                                        <a class="page-link" href="#"><span
                                                                class="fas fa-angle-right"></span></a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
