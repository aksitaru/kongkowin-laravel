{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.user.app')

@push('scripts')
    <script>
        $(function() {
            $('#btn-claim').click(function(e) {
                e.preventDefault();

                var url = '{{ route('place.barcode', [$place, $code_visit]) }}';

                var btn = $(this);
                var rating = btn.data('rate');

                var data = new FormData();
                data.append('_token', '{{ csrf_token() }}');
                data.append('rating', rating);

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: data,
                    beforeSend: function(data) {
                        Swal.fire("Loading", "Mohon Tunggu", "");
                        Swal.showLoading();
                    },
                    success: function(data) {
                        try {
                            Swal.close();
                            if (data.result == true) {
                                $('#claim-body').html(data.data.claim_body);
                                $('#user-point').html(data.data.user_point);

                                Toast.fire(
                                    data.form.alert.head,
                                    data.form.alert.body,
                                    data.form.alert.icon
                                );
                            }

                            if (data.redirect) {
                                location.href = data.redirect;
                            }
                        } catch (error) {
                            Swal.fire("Error Request", error.toString(), "error");
                        }
                    },
                    error: function(request) {
                        try {
                            Swal.close();

                            data = request.responseJSON;
                            if (data.result == false) {
                                if (data.data != undefined) {
                                    if (data.data.login != undefined) {
                                        if (data.data.login == false) {
                                            $('#modal-login').modal('show');
                                        }
                                    }
                                }
                            }

                            if (data.form != undefined) {
                                if (data.form.alert != undefined) {
                                    Swal.fire(
                                        data.form.alert.head,
                                        data.form.alert.body,
                                        data.form.alert.icon
                                    );
                                }
                            }

                            if (data.redirect) {
                                location.href = data.redirect;
                            }
                        } catch (error) {
                            Swal.fire("Error Request", error.toString(), "error");
                        }
                    },
                });

            });

            $('body').on('click', '#btn-exchange', function() {
                Swal.fire({
                    title: 'Apakah anda yakin ingin menukar point anda',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, tukar',
                    cancelButtonText: 'Tidak jadi'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var product_reward_id = $(this).data('product-reward-id');

                        var url = '{{ route('place.barcode.exchange', $place->domain) }}';

                        var data = new FormData();
                        data.append('_token', '{{ csrf_token() }}');
                        data.append('product_reward_id', product_reward_id);

                        $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: data,
                            beforeSend: function(data) {
                                Swal.fire("Loading", "Mohon Tunggu", "");
                                Swal.showLoading();
                            },
                            success: function(data) {
                                $('#user-point').html(data.data.user_point);

                                Swal.fire(
                                    data.form.alert.head,
                                    data.form.alert.body,
                                    data.form.alert.icon
                                );
                            },
                            error: function(request) {
                                data = request.responseJSON;

                                Swal.fire(
                                    data.form.alert.head,
                                    data.form.alert.body,
                                    data.form.alert.icon
                                );
                            },
                        });
                    }
                });
            });
        });
    </script>
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('place.show', $place) }}">{{ $place->name }}</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                </ol>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body text-center">
                                <h3>Terimakasih telah mengunjungi {{ $place->name }}.</h3>

                                <div id="claim-body">
                                    @if ($code_visit == $place->code_visit)
                                        <p>Anda bisa mendapatkan <strong>{{ $place->point_visit }} point</strong>, dengan
                                            menekan
                                            tombol <strong>Claim</strong> dibawah ini.</p>
                                        <button id="btn-claim" class="btn btn-success">Claim</button>
                                        <p><small>* login diperlukan, jika tidak mempunyai akun anda bisa daftar terlebih
                                                dahulu</small></p>
                                    @else
                                        <div class="alert alert-danger" role="alert">
                                            QR Code yang anda scan salah.
                                        </div>
                                        <p><small>* pastikan QR Code terbaru yang anda discan</small></p>
                                    @endif

                                </div>
                                <a href="{{ route('place.show', $place) }}">Ke halaman {{ $place->name }}</a>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <h3>Tukar point</h3>
                                @php
                                    $user_points = 0;
                                @endphp

                                @auth
                                    @php
                                        $sum_point_reward = $place
                                            ->userPlaceRewards()
                                            ->where('user_id', auth()->user()->id)
                                            ->sum('points');
                                        $sum_point_minus_exchange = auth()
                                            ->user()
                                            ->userProductRewards()
                                            ->where('user_id', auth()->user()->id)
                                            ->where('user_product_reward_status_id', '!=', 3)
                                            ->sum('points');
                                        
                                        $user_points = $sum_point_reward - $sum_point_minus_exchange;
                                    @endphp
                                    <p>Point di {{ $place->name }}: <span class="badge badge-success"
                                            id="user-point">{{ $user_points }} Point</span></p>
                                    <p>Tukar point hasil kunjungan anda dengan produk-produk berikut:</p>

                                @endauth
                                @foreach ($place->productRewards as $product_reward)
                                    @php
                                        $product = $product_reward->product;
                                        $product_image = asset('images/empty.jpg');
                                        if ($product->images()->count() > 0) {
                                            $product_image =
                                                '/storage/' .
                                                $product
                                                    ->images()
                                                    ->select('file_path')
                                                    ->limit(1)
                                                    ->get()
                                                    ->pluck('file_path')
                                                    ->toArray()[0];
                                        }
                                        
                                        $progress_claim = round(($user_points / $product_reward->points) * 100, 0);
                                    @endphp
                                    <div class="media">
                                        <img src="{{ $product_image }}" class="img-thumbnail img-80 mr-3">
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col">
                                                    <b>{{ $product->name }}</b>
                                                    <p class="mb-2">
                                                        <span class="badge badge-warning"
                                                            id="user-point">{{ $product_reward->points }} Point</span>
                                                    </p>
                                                </div>
                                                @if ($progress_claim >= 100)
                                                    <div>
                                                        <button id="btn-exchange" class="btn btn-sm btn-primary"
                                                            data-product-reward-id="{{ $product_reward->id }}">Tukar</button>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: {{ $progress_claim }}%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
