@extends('layouts.app')

@section('content')
    <div id="place-section" class="py-3">
        <div class="row align-items-end px-3">
            <h2 class="font-weight-bolder flex-fill flex-lg-fill-unset">Tempat</h2>
            <a href="place" class="animate__animated animate__swing px-2 font-weight-bolder h5">Lihat Semua</a>
        </div>
        <div class="row d-block horizontal-scroll">
            <div class="px-3">
                @foreach ($places as $place)
                    <a href="place/{{ $place->domain }}">
                        <div class="card d-inline-block dashboard-card-place" class="text-decoration-none">
                            <img src="https://exp.cdn-hotels.com/hotels/20000000/19960000/19957000/19956932/ee29a23e_z.jpg"
                                class="card-img-top" alt="{{ $place->name }}">
                            <div class="card-body p-3">
                                <h5 class="card-title">{{ $place->name }}</h5>
                                <div class="place-categories mb-2">
                                    <span class="badge badge-pill badge-primary">Rumah Makan</span>
                                </div>
                                <div class="row mx-0 star-rating align-items-center">
                                    <span class="material-icons-round">
                                        star
                                    </span>
                                    <span class="material-icons-round">
                                        star
                                    </span>
                                    <span class="material-icons-round">
                                        star
                                    </span>
                                    <span class="material-icons-round">
                                        star
                                    </span>
                                    <span class="material-icons-round">
                                        star_outline
                                    </span>
                                    <p class="d-inline ml-2 m-0 text-muted">4 (200)</p>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
