@extends('layouts.base')

@section('baseStyles')
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    {{-- <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet"> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    
@endsection

@section('baseScript')
    <!-- Script -->
    <script src="{{ mix('js/app.js') }}"></script>
    {{-- <script src="{{ asset('assets/select2/js/select2.min.js') }}"></script> --}}

    @stack('scripts')
@endsection

@section('body')
    <x-navbar></x-navbar>

    <div class="container-md">
        @yield('content')
    </div>
    <div class="d-lg-block padding-bottom-mobile"></div>

@endsection