@extends('layouts.user.base')

@section('baseStyles')
    <!-- Styles -->
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

@endsection

@section('baseScript')
    <!-- Script -->
    <script src="{{ mix('js/admin.js') }}"></script>
    <script src="{{ asset('js/forms.js') }}"></script>

    <script src="{{ asset('js/user-auth.js') }}"></script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    @stack('scripts')
@endsection

@section('body')

    <body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
        <div class="wrapper">
            <x-user.modal.login></x-user.modal.login>

            <x-user.navbar></x-user.navbar>
            <x-user.sidebar></x-user.sidebar>

            @yield('content')

            <x-user.footer></x-user.footer>
        </div>
    </body>

@endsection
