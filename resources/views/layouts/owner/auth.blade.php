@extends('layouts.owner.base')

@section('baseStyles')
    <!-- Styles -->
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

@endsection

@section('baseScript')
    <!-- Script -->
    <script src="{{ mix('js/admin.js') }}"></script>
    <script src="{{ asset('js/forms.js') }}"></script>

    @stack('scripts')
@endsection

@section('body')

    <body class="hold-transition">
        <div class="login-page">
            @yield('content')
        </div>
    </body>

@endsection
