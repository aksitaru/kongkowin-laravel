<div style="margin:0;padding:8px;background:#efefef">
    <table style="font-family:'Open Sans','Lato','Calibri';width:100%;background:#efefef;text-align:center">
        <tbody>
            <tr>
                <td>
                    <table style="text-align:center;margin:0 auto">
                        <tbody>
                            <tr>
                                <td style="padding:0px">
                                    <table
                                        style="border:none;border-collapse:collapse;max-width:600px;margin:0 auto;text-align:left;overflow:hidden">
                                        <thead>
                                            <tr colspan="2" height="80px">
                                                <td scope="col" width="25%"> <a href="{{ url('/') }}"
                                                        target="_blank">
                                                        Kongkow.in
                                                        {{-- <img alt=""
                                                            src="https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/marketing_activation_reminder/Final-Logo-Live.On-small-with-gap.png"
                                                            align="left" height="30%" class="CToWUd"> --}}
                                                    </a> </td>
                                                <td scope="col" width="75%">
                                                    <div
                                                        style="text-align:right;font-family:'Open Sans','Lato','Calibri',sans-serif;font-size:12px;font-weight:600">
                                                        <a style="color:#0099ff;text-decoration:none"
                                                            href="{{ url('/') }}" target="_blank">
                                                            APLIKASI
                                                        </a>
                                                        <span style="margin:0 12px">|</span>
                                                        <a style="color:#0099ff;text-decoration:none"
                                                            href="https://www.instagram.com/{{ $instagram }}"
                                                            target="_blank">
                                                            BANTUAN
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="m_-2115211258047876066block1"
                                                        style="background:#ffffff;color:#000000;font-family:'Open Sans','Lato','Calibri',sans-serif;font-size:16px;font-weight:400;line-height:25px;padding:20px;border-radius:8px;margin-bottom:12px">
                                                        @yield('body')
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" style="padding:0">
                                                    <div style="background:#f7f7f7;border-radius:8px">
                                                        <p
                                                            style="color:#666666;font-size:12px;font-family:'Open Sans','Lato','Calibri',sans-serif;margin:0;padding:20px;text-align:center;margin-bottom:12px">
                                                            Jika kamu memiliki pertanyaan lebih lanjut, hubungi kami
                                                            melalui live chat di <a href="{{ url('/') }}"
                                                                style="color:#0099ff" target="_blank">
                                                                website
                                                            </a>
                                                            atau <a href="https://www.instagram.com/{{ $instagram }}"
                                                                style="color:#0099ff" target="_blank">
                                                                Instagram
                                                            </a>.
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"
                                                    style="color:#666666;padding:0px 30px 15px;text-align:center"
                                                    height="100px">
                                                    <p
                                                        style="color:#666666;font-size:12px;font-family:'Open Sans','Lato','Calibri',sans-serif">
                                                        2021 © Kongkow.in
                                                        <br><br>
                                                        <font
                                                            style="font-style:italic;font-size:12px;font-family:'Open Sans','Lato','Calibri',sans-serif">
                                                            Syarat dan ketentuan berlaku.
                                                        </font>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL"> </div>
</div>
