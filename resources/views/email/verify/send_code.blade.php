@extends('email.layouts.app')

@section('body')
    <p style="font-family:Roboto,sans-serif;font-size:16px;color:rgb(0,0,0);line-height:18px;margin-top:18px">
        Kode verifikasi akun anda adalah:
    </p>
    <div>
        <div dir="ltr" data-smartmail="gmail_signature">
            <div dir="ltr">
                <div dir="ltr">
                    <div dir="ltr">
                        <div dir="ltr">
                            <strong><code>
                                <h2>{{ $code }}</h2>
                                </code>
                            </strong>
                            Segera ketik kode tersebut dan akan otomatis kadaluarsa dalam 10
                            menit jika tidak digunakan. Apabila kode ini tidak bekerja, silakan tekan tombol <strong>“Kirim
                                ulang.”</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p><span style="color:rgb(0,0,0);font-family:Roboto,sans-serif;font-size:14px">Terima
            kasih!</span></p>
    <p style="font-family:'Roboto',sans-serif;font-size:14px;line-height:18px;margin-top:18px;color:#000000">
        Semoga nongkrong semakin asik,<br><strong>Kongkow.in</strong></p>
@endsection
