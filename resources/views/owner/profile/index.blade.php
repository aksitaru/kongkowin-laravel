{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@push('scripts')
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        $(function() {
            if (window.location.href.indexOf('#modal-edit-logo') != -1) {
                $('#modal-edit-logo').modal('show');
            }
            $('.select-multiple').select2({
                theme: 'bootstrap4',
                ajax: {
                    processResults: function(data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });


            var previewNode = document.querySelector("#template")
            previewNode.id = ""
            var previewTemplate = previewNode.parentNode.innerHTML
            previewNode.parentNode.removeChild(previewNode)

            var editLogoDropzone = new Dropzone("form#edit-logo-dropzone", {
                maxFiles: 1,
                init: function() {
                    this.on('addedfile', function(file) {
                        if (this.files.length > 1) {
                            this.removeFile(this.files[0]);
                            document.querySelector("#total-progress .progress-bar").style.width =
                            "0%";
                        } else {
                            $('#before-edit').remove();
                        }
                    });

                    this.on("totaluploadprogress", function(progress) {
                        document.querySelector("#total-progress .progress-bar").style.width =
                            progress + "%"
                    });

                    this.on("sending", function(file) {
                        // Show the total progress bar when upload starts
                        document.querySelector("#total-progress").style.opacity = "1"

                        setTimeout(function(){location.reload()}, 1000);
                    })
                },
                uploadMultiple: false,
                acceptedFiles: 'image/*',
                dictInvalidFileType: 'This form only accepts images.',
                previewTemplate: previewTemplate,
                autoQueue: false, // Make sure the files aren't queued until manually added
                previewsContainer: "#previews", // Define the container to display the previews
                clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
            });


            document.querySelector(".start").onclick = function() {
                editLogoDropzone.enqueueFiles(editLogoDropzone.getFilesWithStatus(Dropzone.ADDED))
            }

            var uploadPhotoDropzone = new Dropzone('form#upload-photo-dropzone', {
                acceptedFiles: 'image/*',
                dictInvalidFileType: 'This form only accepts images.'
            });
        });
    </script>
@endpush
@section('content')
    <div class="modal fade" id="modal-edit-logo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Perbarui Foto Profil</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-primary fileinput-button"><span class="fas fa-plus fa-xs"></span>
                        Upload
                        Foto</button>

                    <div class="display-photo text-center my-3" id="before-edit">
                        <img class="profile-user-img img-fluid img-circle img-160"
                            src="/storage/image/place/xYnFk7wGznBH0BAxIPIye84EpziCsDk7YBVrWo4L.jpg" data-dz-thumbnail />
                    </div>

                    <form id="edit-logo-dropzone" action="{{ route('owner.profile.upload_logo', $place->domain) }}"
                        method="post">
                        @csrf
                        <div id="previews">
                            <div id="template" class="text-center">
                                <div class="display-photo my-3">
                                    <img class="profile-user-img img-fluid img-circle img-160"
                                        src="/storage/image/place/xYnFk7wGznBH0BAxIPIye84EpziCsDk7YBVrWo4L.jpg"
                                        data-dz-thumbnail />
                                </div>

                                <strong class="error text-danger" data-dz-errormessage></strong>
                                <div class="d-flex align-items-center">
                                    <div class="fileupload-process w-100">
                                        <div id="total-progress" class="progress progress-striped active" role="progressbar"
                                            aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"
                                                data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" type="submit" class="btn btn-primary start">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard', $place->domain) }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <a data-target="#modal-edit-logo" data-toggle="modal" href="#">
                                        @if ($place->logo_path != null)
                                            <img class="profile-user-img img-fluid img-circle img-80"
                                                src="/storage/{{ $place->logo_path }}">
                                        @else
                                            <img class="profile-user-img img-fluid img-circle img-80"
                                                src="{{ asset('images/empty.jpg') }}">
                                        @endif
                                    </a>
                                </div>

                                <h3 class="profile-username text-center">{{ $place->name }}</h3>

                                {{-- <p class="text-muted text-center">{{ $place->description }}</p> --}}

                                {{-- <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Followers</b> <a class="float-right">1,322</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Following</b> <a class="float-right">543</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Friends</b> <a class="float-right">13,287</a>
                                    </li>
                                </ul>

                                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Tentang Tempat</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">{{ $place->address }}</p>

                                <hr>

                                <strong><i class="fas fa-pencil-alt mr-1"></i> Kategori</strong>

                                <p class="text-muted">
                                    @foreach ($place->categories as $category)
                                        <span class="badge badge-primary">{{ $category->name }}</span>
                                    @endforeach
                                </p>

                                <hr>

                                <strong><i class="fas fa-pencil-alt mr-1"></i> Fasilitas</strong>

                                <p class="text-muted">
                                    @foreach ($place->facilities as $facility)
                                        <span class="badge badge-primary">{{ $facility->name }}</span>
                                    @endforeach
                                </p>

                                <hr>

                                <strong><i class="far fa-file-alt mr-1"></i> Deskripsi</strong>

                                <p class="text-muted">{{ $place->description }}</p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link active" href="#activity"
                                            data-toggle="tab">Foto Tempat</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#timeline"
                                            data-toggle="tab">Posting</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#settings"
                                            data-toggle="tab">Settings</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        <h3>Upload Foto</h3>
                                        <form id="upload-photo-dropzone"
                                            action="{{ route('owner.profile.upload', $place->domain) }}"
                                            class="dropzone" method="post" name="file">
                                            @csrf
                                        </form>
                                        @foreach ($place->images as $image)
                                            <img src="/storage/{{ $image->file_path }}"
                                                style="width: 200px; height: 200px" class="img-thumbnail">
                                        @endforeach
                                    </div>
                                    <div class="tab-pane" id="timeline">
                                    </div>
                                    <div class="tab-pane" id="settings">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
