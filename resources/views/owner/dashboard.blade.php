{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <!-- New Place -->
                <div class="row">
                    <div class="col-lg-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-auto text-center">
                                        <img src="{{ asset('images/product.png') }}" class="align-self-center mr-3">
                                    </div>
                                    <div class="col">
                                        <h3>Tambah Produk Pertamamu</h3>
                                        <p>Mulai Berjualan dengan menambah produk didalam tempat.</p>
                                        <a href="{{ route('owner.product.create', $place->domain) }}" class="btn btn-outline-primary">Tambah Produk</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-auto text-center">
                                        <img src="{{ asset('images/place.png') }}" class="align-self-center mr-3">
                                    </div>
                                    <div class="col">
                                        <h3>Pasang Logo Tempatmu</h3>
                                        <p>Dengan memasang logo, tempat akan semakin terlihat terpercaya.</p>
                                        <a href="{{ route('owner.profile', $place->domain) }}#modal-edit-logo" class="btn btn-outline-primary">Pasang Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
