{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@push('scripts')
    <script type="text/javascript">
        $(function() {

        });
    </script>
@endpush
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard', $place->domain) }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <div class="mb-3 d-flex justify-content-center">{!! DNS2D::getBarcodeHTML(route('place.barcode', [$place, $code]), 'QRCODE') !!}</div>
                                </div>
                                <h3 class="profile-username text-center">{{ $code }}</h3>
                                <div class="text-center">
                                    <a href="{{ route('owner.barcode.next', $place->domain) }}" class="btn btn-success">Next Code</a>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
