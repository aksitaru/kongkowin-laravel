{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('.select2').select2({
                theme: 'bootstrap4',
                ajax: {
                    url: '{{ route('owner.product.select2', $place->domain) }}',
                    processResults: function(data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });

            var table = $('#table1').DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: "{{ route('owner.exchange.list', $place->domain) }}",
                    type: "POST",
                    data: function(d) {
                        d._token = '{{ csrf_token() }}'
                    },
                },
                order: [],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'created_at',
                        name: 'user_product_rewards.created_at',
                    },
                    {
                        data: 'user.name',
                        name: 'user.name',
                    },
                    {
                        data: 'product_html',
                        name: 'product_reward.product.name'
                    },
                    {
                        data: 'points',
                        name: 'user_product_rewards.points'
                    },
                    {
                        data: 'status_html',
                        name: 'user_product_reward_status.name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
        });

        $('body').on('click', '#btn-accept-row', function() {
            var id = $(this).data('id');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success ml-3',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Apakah kamu yakin ingin mengkormasi?',
                text: "Tindakan ini tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Konfirmasi',
                cancelButtonText: 'Ga jadi deh',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#accept-row').attr('action',
                        '{{ route('owner.exchange', $place->domain) }}/accept/' +
                        id);

                    $('#accept-row').submit();
                }
            })
        });

        $('body').on('click', '#btn-cancel-row', function() {
            var id = $(this).data('id');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success ml-3',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Apakah kamu yakin ingin mengbatalkan?',
                text: "Tindakan ini tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Batalkan',
                cancelButtonText: 'Ga jadi deh',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#cancel-row').attr('action',
                        '{{ route('owner.exchange', $place->domain) }}/cancel/' +
                        id);

                    $('#cancel-row').submit();
                }
            })
        });
    </script>
@endpush
@section('content')
    <div class="modal fade" id="modal-add-row">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah ...</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" data-modal='#modal-add-row' data-table='#table1'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product">Pilih Produk</label>
                            <select id="product" class="form-control select2" name="product_id"></select>
                        </div>
                        <div class="form-group">
                            <label for="points">Point yang harus dipenuhi</label>
                            <input id="points" type="number" class="form-control" name="points" min="1" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <form id="accept-row" method="POST" class="d-none" data-table="#table1">@csrf</form>
    <form id="cancel-row" method="POST" class="d-none" data-table="#table1">@csrf</form>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard', $place->domain) }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <a id="btn-add-row" class="btn btn-primary mb-3" href="{{ route('owner.product.reward', $place->domain) }}#add">
                                    <i class="fa fa-plus"></i>
                                    Tambah Reward
                                </a>

                                <div class="table-responsive">
                                    <table id="table1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Waktu</th>
                                                <th>Name</th>
                                                <th>Produk</th>
                                                <th>Point</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
