{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('.select-multiple').select2({
                theme: 'bootstrap4',
                ajax: {
                    url: '{{ route('owner.product.tag.select2', $place->domain) }}',
                    processResults: function(data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });


            var table = $('#table1').DataTable({
                serverSide: true,
                processing: true,
                // scrollX: true,
                ajax: {
                    url: "{{ route('owner.product.list', $place->domain) }}",
                    type: "POST",
                    data: function(d) {
                        d._token = '{{ csrf_token() }}'
                    },
                },
                order: [],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'thumbnail',
                        name: 'thumbnail',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'price',
                        name: 'price'
                    },
                    {
                        data: 'tags',
                        name: 'tags',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
        });

        $('#btn-add-row').click(function() {
            location.href = '{{ route('owner.product.create', $place->domain) }}'
        });

        $('body').on('click', '#btn-edit-row', function() {
            var id = $(this).data('id');

            location.href = '{{ route('owner.product', $place->domain) }}/update/' + id
        });

        $('body').on('click', '#btn-delete-row', function() {
            var id = $(this).data('id');

            var url = '{{ route('owner.product', $place->domain) }}/show/' + id;

            var data = new FormData();
            data.append('_token', '{{ csrf_token() }}');

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                beforeSend: function(data) {
                    Swal.fire("Loading", "Mohon Tunggu", "");
                    Swal.showLoading();
                },
                success: function(data) {
                    Swal.close();

                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success ml-3',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        reverseButtons: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#delete-row').attr('action',
                                '{{ route('owner.product', $place->domain) }}/delete/' +
                                id);

                            $('#delete-row').submit();
                        }
                    })
                },
                error: function(data) {
                    Swal.fire("Error Request", "Coba beberapa saat lagi.", "error");
                },
            });
        });
    </script>
@endpush
@section('content')
    <div class="modal fade" id="modal-add-row">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah ...</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" data-modal='#modal-add-row' data-table='#table1'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nama Produk</label>
                            <input id="name" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi Produk</label>
                            <textarea id="description" class="form-control" name="description" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="price">Harga Produk</label>
                            <input id="price" type="number" class="form-control" name="price" required>
                        </div>
                        <div class="form-group">
                            <label for="tags">Pilih Etalase</label>
                            <select id="tags" class="form-control select-multiple" name="tags[]" multiple></select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-edit-row">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit ...</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" data-modal='#modal-edit-row' data-table='#table1'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nama Produk</label>
                            <input id="name" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi Produk</label>
                            <textarea id="description" class="form-control" name="description" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="price">Harga Produk</label>
                            <input id="price" type="number" class="form-control" name="price" required>
                        </div>
                        {{-- <div class="form-group">
                            <label for="tags">Pilih Etalase</label>
                            <select id="tags" class="form-control select-multiple" name="tags[]" multiple></select>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <form id="delete-row" method="POST" class="d-none" data-table="#table1">@csrf</form>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard', $place->domain) }}">Home</a></li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <button id="btn-add-row" class="btn btn-primary mb-3">
                                    <i class="fa fa-plus"></i>
                                    Tambah Produk
                                </button>

                                <table id="table1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Thumbnail</th>
                                            <th>Name</th>
                                            <th>Harga</th>
                                            <th>Etalase</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
