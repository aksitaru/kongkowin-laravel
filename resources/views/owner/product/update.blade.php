{{-- https://getbootstrap.com/docs/4.5/examples/dashboard/# --}}

@extends('layouts.owner.app')

@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('.select-multiple').select2({
                theme: 'bootstrap4',
                ajax: {
                    url: '{{ route('owner.product.tag.select2', $place->domain) }}',
                    processResults: function(data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });


            Dropzone.options.myGreatDropzone = { // camelized version of the `id`
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 2, // MB
            };
        });
    </script>
@endpush
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard', $place->domain) }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('owner.product', $place->domain) }}">Produk</a></li>
                            <li class="breadcrumb-item active">{{ $title }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('owner.product.update', [$place->domain, $product]) }}"
                                    method="POST">
                                    @csrf
                                    <h3>Informasi Produk</h3>
                                    <div class="form-group">
                                        <label for="name">Nama Produk</label>
                                        <input id="name" class="form-control" name="name" value="{{ $product->name }}"
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Deskripsi Produk</label>
                                        <textarea id="description" class="form-control" name="description" rows="7"
                                            required>{{ $product->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">Harga Produk</label>
                                        <input id="price" type="number" class="form-control" name="price"
                                            value="{{ $product->price }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="tags">Pilih Etalase</label>
                                        <select id="tags" class="form-control select-multiple" name="tags[]" multiple>
                                            @foreach ($product->tags as $tag)
                                                <option value="{{ $tag->tag_id }}" selected>
                                                    {{ App\Models\Tag::find($tag->tag_id)->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Simpan Produk</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h3>Upload Foto</h3>
                                <form action="{{ route('owner.product.upload', [$place->domain, $product]) }}"
                                    class="dropzone" id="my-great-dropzone">
                                    @csrf
                                </form>

                                @foreach ($product->images as $image)
                                    <img src="/storage/{{ $image->file_path }}" style="width: 200px; height: 200px"
                                        class="img-thumbnail">
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
